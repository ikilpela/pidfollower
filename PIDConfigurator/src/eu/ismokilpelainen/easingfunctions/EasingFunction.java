package eu.ismokilpelainen.easingfunctions;

import java.awt.geom.GeneralPath;
import java.util.Set;

/**
 * This is the interface that defines easing functions. To implement your own EasingFunctions you can extend {@link AbstractEasingFunction}.
 * 
 * @author Ismo Kilpeläinen
 *
 */
public interface EasingFunction {
	
	/**
	 * Evaluates response (Y) to input (X)
	 * @param x Input value
	 * @return Response value
	 */
	public float evaluateY(float x);
	
	/**
	 * Returns graph of the function.
	 * Graph contains given number of points distributed equally between 0 and xmax.
	 * 
	 * @param xmax Largest input value to include in the graph
	 * @param points Number of points to include in the graph
	 * @return Path object of the plotted graph
	 */
	public GeneralPath getPath(float xmax, int points);
	
	/**
	 * Returns graph of the function.
	 * Graph contains given number of points distributed equally between xmin and xmax.
	 * 
	 * @param xmin Smallest input value to include
	 * @param xmax Largest input value to include
	 * @param points Number of points to include in the graph
	 * @return Path object of the plotted graph
	 */
	public GeneralPath getPath(float xmin, float xmax, int points);
	

	/**
	 * Returns a set containing all parameter keys for the function.
	 * @return Set of keys for function parameters
	 */
	public Set<String> getParamKeys();
	
	/**
	 * Sets a function parameter.
	 * 
	 * @param key Key of the parameter
	 * @param value Value to set
	 */
	public void setParam(String key, float value);
	
	/**
	 * Returns parameter value.
	 * 
	 * @param key Key of the parameter
	 * @return Value of the parameter
	 */
	public float getParam(String key);
	
	/**
	 * Gets a written representation of the function in HTML format.
	 * 
	 * @return The function as HTML string
	 */
	public String getFormulaHTML();
}
