package eu.ismokilpelainen.easingfunctions;

import eu.ismokilpelainen.easingfunctions.AbstractEasingFunction;

public class PIDFunction extends AbstractEasingFunction {

	
	public PIDFunction(float targetValue, float kp, int mvLimitHigh, int mvLimitLow, float rampThreshold, float rampPower) {
		super();
		this.params.put("TargetValue",targetValue);
		this.params.put("Kp", kp);
		this.params.put("MVLimitHigh", (float)mvLimitHigh);
		this.params.put("MVLimitLow", (float)mvLimitLow);
		this.params.put("RampThreshold", rampThreshold);
		this.params.put("RampPower", rampPower);	
	}

	@Override
	public float evaluateY(float x) {
		float kp = params.get("Kp");
		float targetValue = params.get("TargetValue");
		float mvLimitHigh = params.get("MVLimitHigh");
		float mvLimitLow = params.get("MVLimitLow");
		float outputMV = rampOut(kp * (x-targetValue));
		return outputMV > mvLimitHigh ? mvLimitHigh : (outputMV < mvLimitLow ? mvLimitLow : outputMV);
	}

	@Override
	public String getFormulaHTML() {
		return "<html><b>PIDControl ramp</b><html>";
	}

    private float rampOut(float ov){
    	float rampThreshold = params.get("RampThreshold");
    	float rampPower = params.get("RampPower");
    	double rampExtent = Math.pow(rampThreshold, rampPower);
        if (rampPower==0 || rampThreshold==0) return ov;
        if (Math.abs(ov)>rampThreshold) return ov;
        int workingOV;
        workingOV=(int)(Math.pow(Math.abs(ov), rampPower) / rampExtent * rampThreshold);
        return (ov<0)?-1*workingOV:workingOV;
    }
    
    @Override
    public String toString() {
    	return "PIDControl ramp";
    }
}
