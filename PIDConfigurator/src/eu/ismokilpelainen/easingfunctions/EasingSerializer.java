package eu.ismokilpelainen.easingfunctions;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.HashMap;

import javax.naming.OperationNotSupportedException;

/**
 * This class provides basic functionality to send and receive Linear, Quadratic and Cubic EasingFunctions
 * over DataStreams. 
 * 
 * For usage with newer Java versions {@link java.io.ObjectOutputStream} and {@link java.io.ObjectInputStream} should be used.
 * 
 * @author Ismo Kilpeläinen
 *
 */
public class EasingSerializer {
	
	/**
	 * HashMap containing function names and the header bytes representing them.
	 */
	private static final HashMap<String,Byte> TYPE_ENCODINGS = new HashMap<String,Byte>();
	
	static {
		TYPE_ENCODINGS.put("Linear",(byte)0);
		TYPE_ENCODINGS.put("Quadratic",(byte)1);
		TYPE_ENCODINGS.put("Cubic",(byte)2);
	}
	
	/**
	 * Sends an EasingFunction over provided DataOutputStream. See {@link EasingSerializer} definition for supported EasingFunction classes.
	 * 
	 * @param out DataOutputStream used to transfer the data.
	 * @param f EasingFunction to transfer
	 * @throws IOException when transfer fails
	 * @throws OperationNotSupportedException when trying to send an EasingFunction subclass that is not supported.
	 */
	public static void send(DataOutputStream out, EasingFunction f) throws IOException, OperationNotSupportedException {
		switch (f.toString()) {
			case "Linear":
				out.write(TYPE_ENCODINGS.get("Linear"));
				out.writeFloat(f.getParam("a"));
				out.flush();
				break;
			case "Quadratic":
				out.write(TYPE_ENCODINGS.get("Quadratic"));
				out.writeFloat(f.getParam("a"));
				out.flush();
				break;			
			case "Cubic":
				out.write(TYPE_ENCODINGS.get("Cubic"));
				out.writeFloat(f.getParam("a"));
				out.flush();
				break;
			default:
				throw new OperationNotSupportedException(
						"Easing function serialization not supported for "
				+ f.toString()
				);
		}
	}
	
	/**
	 * Attempts to read EasingFunction from DataInputStream
	 * @param input DataInputStream to read
	 * @return Received EasingFunction
	 * @throws IOException when header byte is not recognized.
	 */
	public static EasingFunction receive(DataInputStream input) throws IOException {
		byte type = input.readByte();
		switch (type) {
		case 0:
			float al = input.readFloat();
			return new LinearEasing(al);
		case 1:
			float aq = input.readFloat();
			return new QuadraticEasing(aq);	
		case 2:
			float ac = input.readFloat();
			return new CubicEasing(ac);
		default:
			throw new IOException("Invalid header byte");
		}
	}
}
