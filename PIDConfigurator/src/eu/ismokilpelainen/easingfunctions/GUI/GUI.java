package eu.ismokilpelainen.easingfunctions.GUI;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import eu.ismokilpelainen.easingfunctions.PIDFunction;
import eu.ismokilpelainen.easingfunctions.EasingFunction;
import eu.ismokilpelainen.easingfunctions.LinearEasing;

import javax.swing.JSplitPane;
import java.awt.BorderLayout;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;

import java.awt.Component;
import java.awt.Dimension;

import net.miginfocom.swing.MigLayout;
import javax.swing.SwingConstants;
import javax.swing.JTextField;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.text.NumberFormat;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.GridLayout;

public class GUI extends JFrame {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private JTextField xminField;
	private JTextField yminField;
	private JTextField xmaxField;
	private JTextField ymaxField;
	
	private static EasingFunction[] functions = new EasingFunction[] {
			new PIDFunction(50f,2.4f,200,-200,100f,3f)
	};
	
	private static NumberFormat paramFormat = NumberFormat.getNumberInstance();
	
	private GraphPanel graphPanel;
	private JComboBox<EasingFunction> comboBox;
	private JPanel paramsPanel;
	
	public GUI() {
		this(new LinearEasing(1f),-10,10,-10,10);
	}
	
	public GUI(EasingFunction f, int xmin, int xmax, int ymin, int ymax) {
		
		JSplitPane splitPane = new JSplitPane();
		getContentPane().add(splitPane, BorderLayout.CENTER);
		
		JPanel panel = new JPanel();
		splitPane.setLeftComponent(panel);
		panel.setLayout(new MigLayout("", "[::10][grow][40.00][40.00]", "[11.00][][][][][][][]"));
				
		JLabel lblMin = new JLabel("Min");
		panel.add(lblMin, "cell 2 0,alignx center,aligny bottom");
		
		JLabel lblMax = new JLabel("Max");
		panel.add(lblMax, "cell 3 0,alignx center,aligny bottom");
		
		JLabel lblInput = new JLabel("Input (X)");
		lblInput.setHorizontalAlignment(SwingConstants.CENTER);
		panel.add(lblInput, "cell 1 1,alignx trailing");

		xminField = new JTextField();		
		panel.add(xminField, "cell 2 1,growx");
		xminField.setColumns(10);
		xminField.setName("xmin");
		xminField.setText(String.valueOf(xmin));
				
		xmaxField = new JTextField();
		panel.add(xmaxField, "cell 3 1,growx");
		xmaxField.setColumns(10);
		xmaxField.setName("xmax");
		xmaxField.setText(String.valueOf(xmax));
		
		JLabel lblY = new JLabel("Response (Y)");
		panel.add(lblY, "cell 1 2,alignx trailing");
		
		yminField = new JTextField();
		panel.add(yminField, "cell 2 2,growx");
		yminField.setColumns(10);
		yminField.setName("ymin");
		yminField.setText(String.valueOf(ymin));
		
		ymaxField = new JTextField();
		panel.add(ymaxField, "cell 3 2,growx");
		ymaxField.setColumns(10);
		ymaxField.setName("ymax");
		ymaxField.setText(String.valueOf(ymax));
		
		JButton btnNewButton = new JButton("Redraw");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				graphPanel.setXmin(Integer.parseInt(xminField.getText()));
				graphPanel.setYmin(Integer.parseInt(yminField.getText()));
				graphPanel.setXmax(Integer.parseInt(xmaxField.getText()));
				graphPanel.setYmax(Integer.parseInt(ymaxField.getText()));
			}
		});
		panel.add(btnNewButton, "cell 2 3 2 1");
		
		JLabel lblParameters = new JLabel("Parameters");
		panel.add(lblParameters, "cell 1 5");
		
		paramsPanel = new JPanel();
		panel.add(paramsPanel, "flowx,cell 1 6 3 1,growx,aligny top");
		paramsPanel.setLayout(new GridLayout(0, 2, 0, 0));
		
		JButton button = new JButton("Set parameters");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				for (Component c : paramsPanel.getComponents()) {
					if (c instanceof JFormattedTextField) {
						JFormattedTextField tf = (JFormattedTextField)c;
						graphPanel.getFunction().setParam(tf.getName(),Float.parseFloat(String.valueOf(tf.getValue())));
						graphPanel.repaint();
					}
				}
			}
		});
		panel.add(button, "cell 1 7 3 1,alignx right");
		
		JPanel rightPanel = new JPanel();
		splitPane.setRightComponent(rightPanel);
		rightPanel.setLayout(new BorderLayout(0, 0));
		
		graphPanel = new GraphPanel(f,xmin,xmax,ymin,ymax);
		rightPanel.add(graphPanel);
		for (String key : f.getParamKeys()) {
			paramsPanel.add(new JLabel(key));
			JFormattedTextField tf = new JFormattedTextField(paramFormat);
			tf.setValue(f.getParam(key));
			tf.setName(key);
			paramsPanel.add(tf);
		}
		graphPanel.setPreferredSize(new Dimension(400,400));
//		addPremadeFunctions();
	}
	
	public void setFunction(EasingFunction f) {
		graphPanel.setFunction(f);
		paramsPanel.removeAll();
		for (String key : f.getParamKeys()) {
			paramsPanel.add(new JLabel(key));
			JFormattedTextField tf = new JFormattedTextField();
			tf.setValue(f.getParam(key));
			tf.setName(key);
			paramsPanel.add(tf);
		}
		paramsPanel.revalidate();
	}
	
//	private void addPremadeFunctions() {
//		for (EasingFunction f : functions) {
//			comboBox.addItem(f);
//		}
//	}
}
