package eu.ismokilpelainen.easingfunctions.GUI;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.font.FontRenderContext;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;
import java.awt.geom.GeneralPath;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.beans.PropertyChangeSupport;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import eu.ismokilpelainen.easingfunctions.EasingFunction;
import eu.ismokilpelainen.easingfunctions.LinearEasing;

public class GraphPanel extends JPanel {

	private EasingFunction function;
	private Rectangle graphArea;
	private int padding = 35;
	
	private int xmin = -10;
	private int xmax = 10;
	private int ymin = 0;
	private int ymax = 10;
	
	private int tickCount = 10;
	private float[] tickmarksY;
	private float[] tickmarksX;
	
	Rectangle highlightRectangle = null;
	Ellipse2D highlightCircle = null;
	
	
	JLabel pointInfo = new JLabel();
	JLabel lblFunction = new JLabel();
	
	PropertyChangeSupport pcs = new PropertyChangeSupport(this);
	
	/**
	 * Create the panel.
	 */
	public GraphPanel() {
		this(new LinearEasing(0.1f),10,10,0,10);
	}
	
	public GraphPanel(EasingFunction f) {
		this(f,-10,10,0,10);
	}

	public GraphPanel(EasingFunction f, int xmin, int xmax, int ymin, int ymax) {
		super();
		this.function = f;
		this.xmin = xmin;
		this.xmax = xmax;
		this.ymin = ymin;
		this.ymax = ymax;
		
		this.setPreferredSize(new Dimension(200,200));
		calculateTickmarks();
		this.setLayout(null);
		this.add(pointInfo);
		this.add(lblFunction);

		lblFunction.setHorizontalAlignment(SwingConstants.CENTER);
		lblFunction.setVerticalAlignment(SwingConstants.CENTER);
		lblFunction.setFont(lblFunction.getFont().deriveFont(Font.BOLD,16f));


		System.out.println(lblFunction.getSize());
		
		this.addComponentListener(new ComponentAdapter() {
			@Override
			public void componentResized(ComponentEvent e) {
				if (highlightRectangle != null) {
					highlightRectangle.setSize(new Dimension(2,getHeight()-2*padding));
				}
				lblFunction.setSize(new Dimension(getWidth(),30));
			}
		});
		
		this.addMouseMotionListener(new MouseAdapter() {
			@Override
			public void mouseExited(MouseEvent e) {
				highlightCircle = null;
				highlightRectangle = null;
			}
			@Override
			public void mouseMoved(MouseEvent e) {
				if (graphArea.contains(e.getPoint())) {
					Point2D graphPoint = screenPointToGraph(e.getPoint());
					float y = yCoordinateOnScreen(graphPoint);
					
					if (highlightRectangle == null) {
						highlightRectangle = new Rectangle( e.getX()-1,graphArea.y,2,graphArea.height);
						highlightCircle = new Ellipse2D.Float(e.getX()-5,y-5, 10, 10);
						repaint(highlightRectangle);
						repaint(highlightCircle.getBounds());
					} else {
						repaint(highlightRectangle);
						repaint(highlightCircle.getBounds());
						highlightRectangle.setLocation(e.getX()-2,(int)highlightRectangle.getY());
						highlightCircle = new Ellipse2D.Float(e.getX()-5, y-5, 10, 10);
						repaint(highlightCircle.getBounds());
						repaint(highlightRectangle);
					}
					pointInfo.setText(round((float)graphPoint.getX(),2) +
							" : " + 
							round((float)graphPoint.getY(),2)
							);
					pointInfo.setSize(new Dimension(80,40));
					int pointInfoX = e.getX() <= getWidth()/2 ? e.getX()+20 : e.getX()-100;
					int pointInfoY = Math.round(y-(y*80/getHeight()));
					pointInfo.setLocation(pointInfoX,pointInfoY);
					pointInfo.repaint();
				}
			}
		});
	}
	
	@Override
	public void paint(Graphics g) {
		super.paint(g);
		
		if (function == null || !this.isShowing() || this.getWidth() <= 0 || this.getHeight() <= 0) {
			return;
		}
		
		calculateGraphArea();
		Graphics2D g2d = (Graphics2D)g;
		
		g2d.setColor(Color.white);
		g2d.fill(graphArea);
		g2d.setColor(Color.black);
		g2d.draw(graphArea);
		
		
		/**
		 *  Draw X-axis tickmarks
		 */
		double y = graphArea.getMaxY();
		int tickNumber = 0;
		double stepX = graphArea.width / (tickCount);
		for (double x = graphArea.getX(); x <= graphArea.getMaxX(); x+=stepX) {
			g2d.drawLine((int)x, (int)y-4, (int)x, (int)y+4);
			centerString(g, 
					new Rectangle((int)x-5,(int)y+15,10,10),
					String.valueOf(tickmarksX[tickNumber]),
					g2d.getFont());
			tickNumber++;
		}
		
		/**
		 * Draw Y-axis tickmarks
		 */
		double stepY = graphArea.height/(tickCount);
		double x = padding;
		tickNumber = 0;
		for (double y0 = graphArea.getMaxY(); y0 >= graphArea.getMinY(); y0-=stepY) {
			g2d.setColor(Color.LIGHT_GRAY);
			g2d.drawLine((int)x, (int)y0, (int)(x+graphArea.width), (int)y0);
			g2d.setColor(Color.black);
			g2d.drawLine((int)x-4, (int)y0, (int)x+4, (int)y0);
			centerString(g, 
					new Rectangle((int)x-25,(int)y0-5,10,10),
					String.valueOf(Math.round(tickmarksY[tickNumber])),
					g2d.getFont());
			tickNumber++;
		}
		
		g2d.setClip(graphArea);
		
		if (highlightRectangle != null) {
			g2d.setColor(new Color(0.8f,0f,0f,0.5f));
			g2d.fill(highlightRectangle);
		}
		
		if (highlightCircle != null) {
			g2d.setColor(new Color(0,0.8f,0));
			g2d.fill(highlightCircle);
		}
		super.paintChildren(g2d);
		
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2d.setColor(Color.BLACK);
		float scaleX = (float)graphArea.width/(xmax-xmin);
		float scaleY = (float)graphArea.height/(ymax-ymin);
		
		GeneralPath path = function.getPath(xmin, xmax, 1000);
		path.transform(AffineTransform.getTranslateInstance(-xmin,-ymax));
		path.transform(AffineTransform.getScaleInstance(scaleX,-scaleY));
		path.transform(AffineTransform.getTranslateInstance(padding,padding));
		g2d.draw(path);		
	}
	
	private void centerString(Graphics g, Rectangle r, String s, 
	        Font font) {
	    FontRenderContext frc = 
	            new FontRenderContext(null, true, true);

	    Rectangle2D r2D = font.getStringBounds(s, frc);
	    int rWidth = (int) Math.round(r2D.getWidth());
	    int rHeight = (int) Math.round(r2D.getHeight());
	    int rX = (int) Math.round(r2D.getX());
	    int rY = (int) Math.round(r2D.getY());

	    int a = (r.width / 2) - (rWidth / 2) - rX;
	    int b = (r.height / 2) - (rHeight / 2) - rY;

	    g.setFont(font);
	    g.drawString(s, r.x + a, r.y + b);
	}

	public int getTickCount() {
		return tickCount;
	}

	public void setTickCount(int tickCount) {
		this.tickCount = tickCount;
		this.repaint();
	}

	public EasingFunction getFunction() {
		return function;
	}

	public void setFunction(EasingFunction f) {
		System.out.println("Function changed");
		this.function = f;
		lblFunction.setText(f.getFormulaHTML());
		this.repaint();
	}

	public int getXmin() {
		return xmin;
	}

	public void setXmin(int xmin) {
		int oldVal = this.xmin;
		this.xmin = xmin;
		calculateTickmarks();
		pcs.firePropertyChange("xmin", oldVal, xmin);
		this.repaint();
	}

	public int getXmax() {
		return xmax;
	}

	public void setXmax(int xmax) {
		int oldVal = this.xmax;
		this.xmax = xmax;
		calculateTickmarks();
		pcs.firePropertyChange("xmax", oldVal, xmax);
		this.repaint();
	}

	public int getYmin() {
		return ymin;
	}

	public void setYmin(int ymin) {
		int oldVal = this.ymin;
		this.ymin = ymin;
		calculateTickmarks();
		pcs.firePropertyChange("ymin", oldVal, ymin);
		this.repaint();
	}

	public int getYmax() {
		return ymax;
	}

	public void setYmax(int ymax) {
		int oldVal = this.ymax;
		this.ymax = ymax;
		calculateTickmarks();
		pcs.firePropertyChange("ymax", oldVal, ymax);
		this.repaint();
	}
	
	public Point2D.Float screenPointToGraph(Point p) {
		float scaleX = (float)graphArea.width/(xmax-xmin);
		Float x = xmin+(p.x-padding)/scaleX;
		Float y = function.evaluateY(x);
		return new Point2D.Float(x, y);
	}
	
	public int yCoordinateOnScreen(Point2D graphPoint) {
		float scaleY = (float)graphArea.height/(ymax-ymin);
		Double y = graphArea.getMaxY()-(graphPoint.getY()-ymin)*scaleY;
		return (int)Math.round(y);
	}
	
	private void calculateGraphArea() {
		Rectangle bounds = this.getBounds();
		this.graphArea = new Rectangle(padding, padding, bounds.width-padding*2, bounds.height-padding*2);
	}
	
	private void calculateTickmarks() {
		if (graphArea == null) {
			calculateGraphArea();
		}
		double tickStepX = (float)(xmax-xmin)/(tickCount);
		double tickStepY = (float)(ymax-ymin)/(tickCount);
		
		tickmarksX = new float[tickCount+1];
		tickmarksY = new float[tickCount+1];
		
		for (int i=0; i <= tickCount; i++) {
			tickmarksX[i] = round((float) (xmin+tickStepX*i),1);
			tickmarksY[i] = round((float) (ymin+tickStepY*i),1);
		}
	}
	
	private static float round (float value, int precision) {
	    int scale = (int) Math.pow(10, precision);
	    return (float) Math.round(value * scale) / scale;
	}
}
