package PIDConfigurator;

public class PIDConfiguration {
	
	private int delay = 10;
	
	private float kp = 1;
	private float ki = 0;
	private float kd = 0;
	
	private float limitHigh = 200;
	private float limitLow = -200;
	
	private float integralLimitHigh = Float.MAX_VALUE;
	private float integralLimitLow = -Float.MAX_VALUE;
	
	private float deadband = 0;
	private float rampExponent = 0;
	private float rampThreshold = 0;

	public float getKp() {
		return kp;
	}

	public void setKp(float kp) {
		this.kp = kp;
	}

	public float getKi() {
		return ki;
	}

	public void setKi(float ki) {
		this.ki = ki;
	}

	public float getKd() {
		return kd;
	}

	public void setKd(float kd) {
		this.kd = kd;
	}

	public float getLimitHigh() {
		return limitHigh;
	}

	public void setLimitHigh(float limitHigh) {
		this.limitHigh = limitHigh;
	}

	public float getLimitLow() {
		return limitLow;
	}

	public void setLimitLow(float limitLow) {
		this.limitLow = limitLow;
	}

	public float getIntegralLimitHigh() {
		return integralLimitHigh;
	}

	public void setIntegralLimitHigh(float integralLimitHigh) {
		this.integralLimitHigh = integralLimitHigh;
	}

	public float getIntegralLimitLow() {
		return integralLimitLow;
	}

	public void setIntegralLimitLow(float integralLimitLow) {
		this.integralLimitLow = integralLimitLow;
	}

	public int getDelay() {
		return delay;
	}

	public void setDelay(int delay) {
		this.delay = delay;
	}

	public float getDeadband() {
		return deadband;
	}

	public void setDeadband(float deadband) {
		this.deadband = deadband;
	}

	public float getRampExponent() {
		return rampExponent;
	}

	public void setRampExponent(float rampExponent) {
		this.rampExponent = rampExponent;
	}

	public float getRampThreshold() {
		return rampThreshold;
	}

	public void setRampThreshold(float rampThreshold) {
		this.rampThreshold = rampThreshold;
	}
}
