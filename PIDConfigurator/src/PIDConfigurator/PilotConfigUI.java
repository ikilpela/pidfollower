package PIDConfigurator;

import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;
import javax.swing.JLabel;
import java.awt.Font;
import java.text.NumberFormat;

import javax.swing.JFormattedTextField;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.awt.event.ActionEvent;
import java.text.Format;
import javax.swing.JSlider;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;
import javax.swing.border.EtchedBorder;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.MouseWheelListener;
import java.awt.event.MouseWheelEvent;

public class PilotConfigUI extends JPanel {
	
	private JSlider speedField;
	private JSlider accelerationField;
	private JSlider rotateSpeedField;
	private JButton btnTransmit;
	private JButton btnPoll;
	
	private Controller controller;
	private JLabel lblWheelDiameter;
	private JLabel lblTrackWidth;
	private JFormattedTextField wheelDiameterField;
	private JFormattedTextField trackWidthField;
	
	private static final NumberFormat intFormat = NumberFormat.getIntegerInstance();
	private static final NumberFormat floatFormat = NumberFormat.getIntegerInstance();

	private JLabel lblPilotProperties;
	private JLabel lblLeftMotor;
	private JLabel lblRightMotor;
	private JComboBox<String> leftMotorCBox;
	private JComboBox<String> rightMotorCBox;
	private JPanel panel;

	private final JLabel lblAcceleration;
	private final JLabel lblRotateSpeed;
	private final JLabel lblSpeed;
	
	static {
		intFormat.setParseIntegerOnly(true);
		intFormat.setMaximumIntegerDigits(3);
		floatFormat.setMaximumFractionDigits(2);
		floatFormat.setMinimumFractionDigits(1);
	};
	
	public PilotConfigUI() {
		setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
	
		setLayout(new MigLayout("", "[][][417.00,grow][]", "[][][][][20px][][][]"));
		
		JLabel lblPilotConfiguration = new JLabel("Robot speeds");
		lblPilotConfiguration.setFont(new Font("Tahoma", Font.BOLD, 14));
		add(lblPilotConfiguration, "flowx,cell 0 0 3 1");
		
		MouseListener ml = new MouseAdapter() {
		    public void mousePressed(final MouseEvent e) {
		        SwingUtilities.invokeLater(new Runnable() {
		            public void run() {
		                JFormattedTextField tf = (JFormattedTextField)e.getSource();
		                int offset = tf.viewToModel(e.getPoint());
		                tf.setCaretPosition(offset);
		            }
		        });
		    }
		};
		
		JLabel lblSpeedTitle = new JLabel("Speed");
		add(lblSpeedTitle, "cell 1 1,alignx trailing");
		
		lblSpeed = new JLabel("100");
		add(lblSpeed, "cell 3 1");
		
		speedField = new JSlider();
		speedField.addMouseWheelListener(new MouseWheelListener() {
		    @Override
		    public void mouseWheelMoved(MouseWheelEvent e) {
		        int notches = e.getWheelRotation();
		        if (notches < 0) {
		            speedField.setValue(speedField.getValue() + 5);
		        } else {
		            speedField.setValue(speedField.getValue() - 5);
		        }
			}
		});
		speedField.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {				
				JSlider source = (JSlider)arg0.getSource();
				lblSpeed.setText(String.valueOf(source.getValue()));			
				if (controller != null && !source.getValueIsAdjusting()) {
					controller.setPilotSpeed(source.getValue());
					controller.sendPilotParam(StreamHeaders.PILOT_PARAM_SPEED);
				}
			}
		});
		speedField.setMinorTickSpacing(10);
		speedField.setPaintTicks(true);
		speedField.setMajorTickSpacing(50);
		speedField.setValue(100);
		speedField.setMaximum(300);
		add(speedField, "cell 2 1,growx");
				
		JLabel lblAccelerationTitle = new JLabel("Acceleration");
		add(lblAccelerationTitle, "cell 1 2,alignx trailing");
		
		lblAcceleration = new JLabel("100");
		add(lblAcceleration, "cell 3 2");
		
		accelerationField = new JSlider();
		accelerationField.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {				
				JSlider source = (JSlider)arg0.getSource();
				lblAcceleration.setText(String.valueOf(source.getValue()));			
				if (controller != null && !source.getValueIsAdjusting()) {
					controller.setPilotAcceleration(source.getValue());
					controller.sendPilotParam(StreamHeaders.PILOT_PARAM_ACCELERATION);
				}
			}
		});
		accelerationField.setPaintTicks(true);
		accelerationField.setMajorTickSpacing(100);
		accelerationField.setMinorTickSpacing(20);
		accelerationField.setMaximum(600);
		accelerationField.addMouseWheelListener(new MouseWheelListener() {
		    @Override
		    public void mouseWheelMoved(MouseWheelEvent e) {
		        int notches = e.getWheelRotation();
		        if (notches < 0) {
		            accelerationField.setValue(accelerationField.getValue() + 5);
		        } else {
		            accelerationField.setValue(accelerationField.getValue() - 5);
		        }
			}
		});
		add(accelerationField, "cell 2 2,growx");
		

		
		JLabel lblRotateSpeedTitle = new JLabel("Rotate speed");
		add(lblRotateSpeedTitle, "cell 1 3,alignx trailing");
		
		
		lblRotateSpeed = new JLabel("100");
		add(lblRotateSpeed, "cell 3 3");
		
		rotateSpeedField = new JSlider();
		rotateSpeedField.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {				
				JSlider source = (JSlider)arg0.getSource();
				lblRotateSpeed.setText(String.valueOf(source.getValue()));			
				if (controller != null && !source.getValueIsAdjusting()) {
					controller.setPilotRotateSpeed(source.getValue());
					controller.sendPilotParam(StreamHeaders.PILOT_PARAM_ROTATESPEED);
				}
			}
		});
		rotateSpeedField.setMajorTickSpacing(100);
		rotateSpeedField.setMinorTickSpacing(20);
		rotateSpeedField.setPaintTicks(true);
		rotateSpeedField.setMaximum(600);
		rotateSpeedField.addMouseWheelListener(new MouseWheelListener() {
		    @Override
		    public void mouseWheelMoved(MouseWheelEvent e) {
		        int notches = e.getWheelRotation();
		        if (notches < 0) {
		            rotateSpeedField.setValue(rotateSpeedField.getValue() + 5);
		        } else {
		            rotateSpeedField.setValue(rotateSpeedField.getValue() - 5);
		        }
			}
		});
		add(rotateSpeedField, "cell 2 3,growx");
		

		
		panel = new JPanel();
		panel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		add(panel, "cell 1 5 2 2,grow");
		panel.setLayout(new MigLayout("", "[][][][][][][][][grow]", "[][][][][][]"));
		
		lblPilotProperties = new JLabel("PilotProps (requires restart)");
		panel.add(lblPilotProperties, "cell 0 0 2 1");
		lblPilotProperties.setFont(new Font("Tahoma", Font.BOLD, 14));
		
		lblWheelDiameter = new JLabel("Wheel Diameter");
		panel.add(lblWheelDiameter, "cell 0 1,alignx right");
		
		wheelDiameterField = new JFormattedTextField(floatFormat);
		panel.add(wheelDiameterField, "cell 1 1");
		wheelDiameterField.setColumns(6);
		
		lblTrackWidth = new JLabel("Track width");
		panel.add(lblTrackWidth, "cell 0 2,alignx right");
		
		trackWidthField = new JFormattedTextField(floatFormat);
		panel.add(trackWidthField, "cell 1 2");
		trackWidthField.setColumns(6);
		
		lblLeftMotor = new JLabel("Left motor");
		panel.add(lblLeftMotor, "cell 0 3,alignx right");
		
		leftMotorCBox = new JComboBox<String>();
		leftMotorCBox.setModel(new DefaultComboBoxModel<String>(new String[] {"A", "B", "C"}));
		leftMotorCBox.setSelectedIndex(2);
		leftMotorCBox.addItemListener(new ItemListener() {
		    @Override
		    public void itemStateChanged(ItemEvent event) {
		       if (event.getStateChange() == ItemEvent.SELECTED) {
		          char selected = event.getItem().toString().toCharArray()[0];
		          controller.setMotorLeft(selected);
		       }
		    } 
		});
		panel.add(leftMotorCBox, "cell 1 3");
		
		lblRightMotor = new JLabel("Right motor");
		panel.add(lblRightMotor, "cell 0 4,alignx right");
		
		rightMotorCBox = new JComboBox<String>();
		panel.add(rightMotorCBox, "cell 1 4");
		rightMotorCBox.setModel(new DefaultComboBoxModel<String>(new String[] {"A", "B", "C"}));
		rightMotorCBox.setSelectedIndex(0);
		rightMotorCBox.addItemListener(new ItemListener() {
		    @Override
		    public void itemStateChanged(ItemEvent event) {
		       if (event.getStateChange() == ItemEvent.SELECTED) {
		    	   char selected = event.getItem().toString().toCharArray()[0];
		          controller.setMotorRight(selected);
		       }
		    } 
		});
		
		btnPoll = new JButton("Poll NXT");
		panel.add(btnPoll, "cell 5 5");
		btnPoll.setEnabled(false);
		btnPoll.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				controller.sendCommand(StreamHeaders.NXT_POLL_PILOT_CMD);
			}
		});
		
		btnTransmit = new JButton("Transmit");
		panel.add(btnTransmit, "cell 7 5,alignx right");
		btnTransmit.setEnabled(false);
		trackWidthField.addMouseListener(ml);
		trackWidthField.addPropertyChangeListener("value",new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent arg0) {
				controller.setPilotTrackWidth(Float.valueOf(arg0.getNewValue().toString()));
			}
		});
		wheelDiameterField.addMouseListener(ml);
		wheelDiameterField.addPropertyChangeListener("value",new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent arg0) {
				controller.setPilotWheelDiameter(Float.valueOf(arg0.getNewValue().toString()));
			}
		});
	}
	
	public void setConnected(Boolean connected) {
		btnTransmit.setEnabled(connected);
		btnPoll.setEnabled(connected);
	}

	public void registerController(Controller controller) {
		this.controller = controller;
		refreshFields();
	}

	public void refreshFields() {
		speedField.setValue(controller.getPilotSpeed());
		accelerationField.setValue(controller.getPilotAcceleration());
		rotateSpeedField.setValue(controller.getPilotRotateSpeed());
		wheelDiameterField.setValue(controller.getPilotWheelDiameter());
		trackWidthField.setValue(controller.getPilotTrackWidth());
		leftMotorCBox.setSelectedItem(controller.getMotorLeft());
		rightMotorCBox.setSelectedItem(controller.getMotorRight());
	}

}
