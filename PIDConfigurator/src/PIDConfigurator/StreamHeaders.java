package PIDConfigurator;

/**
 * Helper class containing all header and control bytes used in communications 
 * between PIDConfigurator PC-app and PIDFollower NXT robot.
 * 
 * @author Ismo Kilpeläinen
 *
 */
public abstract class StreamHeaders {
	
	// Command type headers 
	/**
	 * Header byte denoting a request for all PID parameters to be sent from NXT to PC.
	 * Same header is also used when NXT replies back to PC. 
	 */
	public static final byte NXT_POLL_PID_CMD = 0;
	
	/**
	 * Header byte denoting an incoming set of PID parameters from PC to NXT.
	 */
	public static final byte SEND_PID_VALUES_CMD = 1;
	
	/**
	 * Header byte denoting a request for all pilot parameters to be sent from NXT to PC.
	 */
	public static final byte NXT_POLL_PILOT_CMD = 2;
	
	/**
	 * Header byte denoting an incoming set of PilotProps paremeters from PC to NXT.
	 */
	public static final byte SEND_PILOT_PROPS_CMD = 3;
	
	/**
	 * Header byte denoting an incoming single PID parameter. The next byte should be 
	 * PID_PARAM_XXX to specify which parameter is sent.
	 */
	public static final byte SEND_SINGLE_PID_PARAM_CMD = 4;
	
	/**
	 * Header byte denoting an incoming single pilot parameter. The next byte should be 
	 * PILOT_PARAM_XXX to specify which parameter is sent.
	 */
	public static final byte SEND_SINGLE_PILOT_PARAM_CMD = 5;
	
	/**
	 * Header byte denoting "Stop PID" command.
	 */
	public static final byte STOP_PID_CMD = 6;
	
	/**
	 * Header byte denoting incoming follower state values.
	 */
	public static final byte SEND_FOLLOWER_STATE_CMD = 7;
	
	/**
	 * Header byte denoting "PIDFollower started" event.
	 */
	public static final byte SEND_FOLLOWER_STARTED_CMD = 8;
	
	/**
	 * Header byte denoting "PIDFollower stopped" event.
	 */
	public static final byte SEND_FOLLOWER_STOPPED_CMD = 9;
	
	// PID param types
	public static final byte PID_PARAM_DELAY = 0;
	public static final byte PID_PARAM_KP = 1;
	public static final byte PID_PARAM_KD = 2;
	public static final byte PID_PARAM_KI = 3;
	public static final byte PID_PARAM_LIMITLOW = 4;
	public static final byte PID_PARAM_LIMITHIGH = 5;
	public static final byte PID_PARAM_DEADBAND = 6;
	public static final byte PID_PARAM_INTEGRALLIMITLOW = 7;
	public static final byte PID_PARAM_INTEGRALLIMITHIGH = 8;
	public static final byte PID_PARAM_RAMPPOWER = 9;
	public static final byte PID_PARAM_RAMPTHRESHOLD = 10;
	
	// PILOT param types
	public static final byte PILOT_PARAM_SPEED = 0;
	public static final byte PILOT_PARAM_ACCELERATION = 1;
	public static final byte PILOT_PARAM_ROTATESPEED = 2;
	public static final byte PILOT_PARAM_WHEELDIAMETER = 3;
	public static final byte PILOT_PARAM_TRACKWIDTH = 4;
}
