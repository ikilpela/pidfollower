package PIDConfigurator;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;

import lejos.nxt.Button;
import lejos.nxt.LCD;
import lejos.nxt.LightSensor;
import lejos.nxt.Motor;
import lejos.nxt.MotorPort;
import lejos.nxt.NXTFrame;
import lejos.nxt.SensorPort;
import lejos.nxt.Sound;
import lejos.nxt.remote.RemoteMotor;

public class NXTPanel extends JPanel {
	
	public static void main(String[] args) {
	}
	
	private static int buttonsPressed = 0;
	// Monitor for button presses
	private static Object monitor = new Object();
	private static NXTFrame singleton = null;
	
	public NXTPanel() {
		super();
		
		JButton enter = new JButton("ENTER");
		JButton escape = new JButton("ESCAPE");
		JButton left = new JButton("<");
		JButton right = new JButton(">");
		
		enter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonsPressed |= Button.ID_ENTER;
				buttonNotify();
			}
		});
		
		escape.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonsPressed |= Button.ID_ESCAPE;
				buttonNotify();
			}
		});
		
		left.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonsPressed |= Button.ID_LEFT;
				buttonNotify();
			}
		});
		
		right.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonsPressed |= Button.ID_RIGHT;
				buttonNotify();
			}
		});
		JPanel content = new JPanel();
		JPanel buttonPanel = new JPanel(new BorderLayout());
		
		buttonPanel.add(enter, BorderLayout.CENTER);
		buttonPanel.add(escape, BorderLayout.SOUTH);
		buttonPanel.add(left, BorderLayout.WEST);
		buttonPanel.add(right, BorderLayout.EAST);

		setSize(250, 300);
	
		LCD lcd = LCD.getSingleton();
        lcd.setMinimumSize(new Dimension(LCD.LCD_WIDTH*2, LCD.LCD_HEIGHT*2));
        lcd.setEnabled(true);
        lcd.setPreferredSize(lcd.getMinimumSize());
        lcd.setBorder(BorderFactory.createEtchedBorder());
		LCD.clear();
		content.add(lcd);
		content.add(buttonPanel);
	}
	/**
	 * Test if a button or set of buttons is pressed
	 * If they are unset them, and return true
	 * 
	 * @param code the button identifier
	 * @return true iff the buttons are pressed
	 */
	public static boolean isPressed(int code) {
		boolean pressed = (buttonsPressed & code) != 0;
		buttonsPressed &= ~code; // unset bits
		// System.out.println("Code:" + code  + ", pressed:" + pressed);
		return pressed;
	}
	
	/**
	 * Get the button mask, which indicates which buttons have been pressed,
	 * but not consumed.
	 * 
	 * @return the button mask
	 */
	public static int getButtons() {
		//System.out.println("getButtons:" + buttonPressed);
		return buttonsPressed;
	}
	
	// Notify all listeners of a button press
	private static void buttonNotify() {
		synchronized(monitor) {
			monitor.notifyAll();
		}
	}
	
	public static int waitForButtons(int timeout) {
		//TODO respect timeout
		synchronized(monitor) {
			try {
				NXTPanel.monitor.wait();
			} catch (InterruptedException e) {
				// Ignore
			}
		}
		//TODO only return the buttons that were pressed since last time
		// not all that are pressed at the moment
		return buttonsPressed;
	}
}
