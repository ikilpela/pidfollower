package PIDConfigurator;

import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.text.NumberFormatter;

import PIDConfigurator.ConnectionStatusView.ConnectionStatus;
import eu.ismokilpelainen.easingfunctions.PIDFunction;
import eu.ismokilpelainen.easingfunctions.GUI.GUI;
import net.miginfocom.swing.MigLayout;
import javax.swing.JLabel;
import java.awt.Font;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.NumberFormat;

import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.ActionEvent;
import java.text.Format;
import javax.swing.border.EtchedBorder;
import java.awt.FlowLayout;

public class PIDConfigUI extends JPanel {
	
	private static final NumberFormat format = NumberFormat.getNumberInstance();
		
	static {
		format.setMinimumFractionDigits(1);
		format.setMaximumFractionDigits(6);
	}
	
	private Controller controller;
	private JFormattedTextField kpField;
	private JFormattedTextField kdField;
	private JFormattedTextField kiField;
	private JFormattedTextField limitLowField;
	private JFormattedTextField limitHighField;
	private JFormattedTextField integralLimitLowField;
	private JFormattedTextField delayField;
	private JFormattedTextField deadbandField;
	private JFormattedTextField rampPowerField;
	private JFormattedTextField rampThresholdField;
	
	private JButton btnConnect;
	private JFormattedTextField integralLimitHighField;
	private JButton btnPoll;
	private JButton btnTransmit;
	private JPanel buttonsPanel;
	private JButton btnNewButton;
	
	public PIDConfigUI() {
		setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		setLayout(new MigLayout("", "[131.00][100.00,grow]", "[][][][][][][][][][20.00][][][grow]"));
		
		MouseListener ml = new MouseAdapter() {
		    public void mousePressed(final MouseEvent e) {
		        SwingUtilities.invokeLater(new Runnable() {
		            public void run() {
		                JFormattedTextField tf = (JFormattedTextField)e.getSource();
		                int offset = tf.viewToModel(e.getPoint());
		                tf.setCaretPosition(offset);
		            }
		        });
		    }
		};
		
		JLabel lblPidConfiguration = new JLabel("PID Configuration");
		lblPidConfiguration.setFont(new Font("Tahoma", Font.BOLD, 14));
		add(lblPidConfiguration, "cell 0 0 2 1");
		
		JLabel lblDelay = new JLabel("Delay");
		add(lblDelay, "cell 0 1,alignx trailing");
		
		NumberFormatter intFormatter = new NumberFormatter(NumberFormat.getIntegerInstance());
		intFormatter.setMinimum(1);
		intFormatter.setMaximum(100);
		delayField = new JFormattedTextField(intFormatter);
		delayField.setColumns(6);
		delayField.addPropertyChangeListener("value",new PropertyChangeListener() {

			@Override
			public void propertyChange(PropertyChangeEvent arg0) {
				controller.setDelay(Integer.valueOf(arg0.getNewValue().toString()));
			}
		});
		delayField.addMouseListener(ml);
		add(delayField, "flowx,cell 1 1,alignx left");
		
		JLabel lblKp = new JLabel("Kp");
		add(lblKp, "cell 0 2,alignx trailing");
		
		kpField = new JFormattedTextField(format);
		kpField.addPropertyChangeListener("value",new PropertyChangeListener() {

			@Override
			public void propertyChange(PropertyChangeEvent arg0) {
				controller.setKp(Float.valueOf(arg0.getNewValue().toString()));
			}
			
		});
		kpField.addMouseListener(ml);
		add(kpField, "cell 1 2,growx");
		
		
		JLabel lblKi = new JLabel("Ki");
		add(lblKi, "cell 0 3,alignx trailing");
		kiField = new JFormattedTextField(format);		
		kiField.addPropertyChangeListener("value",new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent arg0) {
				controller.setKi(Float.valueOf(arg0.getNewValue().toString()));
			}
		});
		kiField.addMouseListener(ml);
		add(kiField, "cell 1 3,growx");

		
		JLabel lblKd = new JLabel("Kd");
		add(lblKd, "cell 0 4,alignx trailing");		
		kdField = new JFormattedTextField(format);
		kdField.addPropertyChangeListener("value",new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent arg0) {
				controller.setKd(Float.valueOf(arg0.getNewValue().toString()));
			}
		});
		kdField.addMouseListener(ml);
		add(kdField, "cell 1 4,growx");
		
		JLabel lblDeadband = new JLabel("Deadband");
		add(lblDeadband, "cell 0 5,alignx trailing");
		
		deadbandField = new JFormattedTextField(format);
		deadbandField.addMouseListener(ml);
		add(deadbandField, "cell 1 5,growx");
		deadbandField.addPropertyChangeListener("value",new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent arg0) {
				controller.setDeadband(Float.valueOf(arg0.getNewValue().toString()));
			}
		});
		
		JLabel lblMvLimitLow = new JLabel("MV Limit Low");
		add(lblMvLimitLow, "cell 0 6,alignx trailing");
		
		limitLowField = new JFormattedTextField(format);
		limitLowField.addMouseListener(ml);
		add(limitLowField, "cell 1 6,growx");
		limitLowField.addPropertyChangeListener("value",new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent arg0) {
				controller.setLimitLow(Float.valueOf(arg0.getNewValue().toString()));
			}
		});
		
		JLabel lblMvLimitHigh = new JLabel("MV Limit High");
		add(lblMvLimitHigh, "cell 0 7,alignx trailing");
		
		limitHighField = new JFormattedTextField(format);
		limitHighField.addMouseListener(ml);
		add(limitHighField, "cell 1 7,growx");
		limitHighField.addPropertyChangeListener("value",new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent arg0) {
				controller.setLimitHigh(Float.valueOf(arg0.getNewValue().toString()));
			}
		});
		
		JLabel lblIntegralLimitLow = new JLabel("Integral Limit Low");
		add(lblIntegralLimitLow, "cell 0 8,alignx trailing");
		
		integralLimitLowField = new JFormattedTextField(format);
		integralLimitLowField.addMouseListener(ml);
		add(integralLimitLowField, "cell 1 8,growx");
		integralLimitLowField.addPropertyChangeListener("value",new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent arg0) {
				controller.setIntegralLimitLow(Float.valueOf(arg0.getNewValue().toString()));
			}
		});
		
		JLabel lblIntegralLimitHigh = new JLabel("Integral Limit High");
		add(lblIntegralLimitHigh, "cell 0 9,alignx trailing");
		
		JLabel lblMs = new JLabel("ms");
		add(lblMs, "cell 1 1");
		
		integralLimitHighField = new JFormattedTextField((Format) null);
		integralLimitHighField.addMouseListener(ml);
		add(integralLimitHighField, "cell 1 9,growx");
		integralLimitHighField.addPropertyChangeListener("value",new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent arg0) {
				controller.setIntegralLimitHigh(Float.valueOf(arg0.getNewValue().toString()));
			}
		});
		
		JLabel lblRampExponent = new JLabel("Ramp Exponent");
		add(lblRampExponent, "cell 0 10,alignx trailing");
		
		rampPowerField = new JFormattedTextField(format);
		rampPowerField.addMouseListener(ml);
		add(rampPowerField, "cell 1 10,growx");
		rampPowerField.addPropertyChangeListener("value",new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent arg0) {
				controller.setRampExponent(Float.valueOf(arg0.getNewValue().toString()));
			}
		});
		
		JLabel lblRampPower = new JLabel("Ramp Threshold");
		add(lblRampPower, "cell 0 11,alignx trailing");
		
		rampThresholdField = new JFormattedTextField((Format) null);
		rampThresholdField.addMouseListener(ml);
		add(rampThresholdField, "cell 1 11,growx");
		rampThresholdField.addPropertyChangeListener("value",new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent arg0) {
				controller.setRampThreshold(Float.valueOf(arg0.getNewValue().toString()));
			}
		});
		
		buttonsPanel = new JPanel();
		add(buttonsPanel, "cell 0 12 2 1,grow");
		buttonsPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		btnPoll = new JButton("Poll NXT");
		buttonsPanel.add(btnPoll);
		btnPoll.setEnabled(false);
		btnPoll.setAlignmentX(0.5f);
		
		btnNewButton = new JButton("Show graph");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JFrame f = new JFrame();
				f.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				PIDFunction function = new PIDFunction(
						50f,
						controller.getKp(),
						(int)controller.getLimitHigh(),
						(int)controller.getLimitLow(),
						controller.getRampThreshold(),
						controller.getRampExponent());
				
				GUI gui = new GUI(
						function,
						0,
						100,
						-200,
						200);
				gui.pack();
				gui.setVisible(true);
//				f.setContentPane(gui);
//				f.pack();
//				f.setVisible(true);
			}
		});
		buttonsPanel.add(btnNewButton);
		
		btnTransmit = new JButton("Transmit");
		buttonsPanel.add(btnTransmit);
		btnTransmit.setEnabled(false);
		btnTransmit.setAlignmentX(0.5f);
		btnTransmit.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				controller.sendCommand(StreamHeaders.SEND_PID_VALUES_CMD);
			}
		});
		btnPoll.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				controller.sendCommand(StreamHeaders.NXT_POLL_PID_CMD);
			}
			
		});
	}

	public void registerController(Controller controller) {
		this.controller = controller;
		refreshFields();
	}
	
	public void refreshFields() {
		delayField.setValue(controller.getDelay());
		kpField.setValue(controller.getKp());
		kdField.setValue(controller.getKd());
		kiField.setValue(controller.getKi());
		deadbandField.setValue(controller.getDeadband());
		limitLowField.setValue(controller.getLimitLow());
		limitHighField.setValue(controller.getLimitHigh());
		integralLimitLowField.setValue(controller.getIntegralLimitLow());
		integralLimitHighField.setValue(controller.getIntegralLimitHigh());
		rampPowerField.setValue(controller.getRampExponent());
		rampThresholdField.setValue(controller.getRampThreshold());
	}

	public void setConnectionStatus(ConnectionStatus status) {
		switch (status) {
		case CONNECTED:
			btnPoll.setEnabled(true);
			btnTransmit.setEnabled(true);
			break;
		case CONNECTING:
			break;
		case DISCONNECTED:
			btnPoll.setEnabled(false);
			btnTransmit.setEnabled(false);
			break;
		default:
			break;	
		}
	}	
}
