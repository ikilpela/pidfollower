package PIDConfigurator;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Objects;

import PIDConfigurator.ConnectionStatusView.ConnectionStatus;
import lejos.pc.comm.NXTCommLogListener;
import lejos.pc.comm.NXTConnector;

public class Controller implements ConnectionController {

	private PIDConfiguration pidModel;
	private PilotConfiguration pilotModel;
	
	private PIDConfigUI pidView;
	private PilotConfigUI pilotView;
	private ConnectionStatusView connectionStatusView;
	private NXTConnector conn = new NXTConnector();
	private NXTConnectionListener listener;
	private boolean connected = false;
	private RobotStatePanel robotStateView;
	private TimerPanel timerPanel;
	
	public Controller(PIDConfiguration pidModel, PilotConfiguration pilotModel, PIDConfigUI pidView, PilotConfigUI pilotView, ConnectionStatusView csv) {
		super();
		this.pidModel = pidModel;
		this.pilotModel = pilotModel;
		this.pidView = pidView;
		this.pilotView = pilotView;
		this.connectionStatusView = csv;
		
		conn.addLogListener(new NXTCommLogListener() {
			public void logEvent(String message) {
				System.out.println("BTSend Log.listener: " + message);
			}

			public void logEvent(Throwable throwable) {
				System.out.println("BTSend Log.listener - stack trace: ");
				throwable.printStackTrace();
			}
		});
	}

	public void connect(final long timeout) {

		pidView.setConnectionStatus(ConnectionStatus.CONNECTING);
		connectionStatusView.setConnectionStatus(ConnectionStatus.CONNECTING);
		Thread t = new Thread(new Runnable() {
			long startTime = System.currentTimeMillis();

			@Override
			public void run() {
				while (!connected && System.currentTimeMillis() - startTime < timeout) {
					connected = conn.connectTo();
				}
				if (connected) {
					pidView.setConnectionStatus(ConnectionStatus.CONNECTED);
					connectionStatusView.setConnectionStatus(ConnectionStatus.CONNECTED);
					System.out.println("Connection status: Connected");
					listener = new NXTConnectionListener(conn, Controller.this);
					new Thread(listener).start();
					sendCommand(StreamHeaders.NXT_POLL_PID_CMD);
					sendCommand(StreamHeaders.NXT_POLL_PILOT_CMD);
					
				} else {
					pidView.setConnectionStatus(ConnectionStatus.DISCONNECTED);
					connectionStatusView.setConnectionStatus(ConnectionStatus.DISCONNECTED);
					System.out.println("No NXT found");
				}

			}
		});
		t.start();
	}
	
	public void connect() {
		connect(Long.MAX_VALUE);
	}

	public void disconnect() {
		try {
			if (listener != null) {
				listener.close();
			}
			conn.close();
			connectionStatusView.setConnectionStatus(ConnectionStatus.DISCONNECTED);
			this.connected = false;
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void updatePIDView() {
		pidView.refreshFields();
	}
	
	public void updatePilotView() {
		pilotView.refreshFields();
	}
	
	public void setRunning(boolean running) {
		if (timerPanel != null) {
			if (running) {
				timerPanel.start();
			} else { 
				timerPanel.stop();
			}
		}	
		if (robotStateView != null) {
			System.out.println("Blaa running : " + running);
			robotStateView.setRunning(running);
		}
	}
	
	public void setRobotStateView(RobotStatePanel robotStateView) {
		this.robotStateView = robotStateView;
		robotStateView.setMaxSpeed(300);
	}
	
	public void setTimerPanel(TimerPanel tp) {
		this.timerPanel = tp;
	}

	public void sendCommand(byte commandType) {
		if (!connected) {
			System.out.println("Not connected, cannot send command " + commandType);
			return;
		}
		
		DataOutputStream dataOut = new DataOutputStream(conn.getOutputStream());
		try {
			switch (commandType) {
			
				case StreamHeaders.NXT_POLL_PID_CMD:
					dataOut.writeByte(StreamHeaders.NXT_POLL_PID_CMD);
					dataOut.flush();
					break;
					
				case StreamHeaders.SEND_PID_VALUES_CMD:
					dataOut.writeByte(StreamHeaders.SEND_PID_VALUES_CMD);
					dataOut.writeInt(getDelay());
					dataOut.writeFloat(getKp());
					dataOut.writeFloat(getKd());
					dataOut.writeFloat(getKi());
					dataOut.writeFloat(getLimitHigh());
					dataOut.writeFloat(getLimitLow());
					dataOut.writeFloat(getIntegralLimitHigh());
					dataOut.writeFloat(getIntegralLimitLow());
					dataOut.writeFloat(getDeadband());
					dataOut.writeFloat(getRampExponent());
					dataOut.writeFloat(getRampThreshold());
					dataOut.flush();
					break;
					
				case StreamHeaders.NXT_POLL_PILOT_CMD: 
					dataOut.writeByte(StreamHeaders.NXT_POLL_PILOT_CMD);
					dataOut.flush();
					break;
				
				case StreamHeaders.SEND_PILOT_PROPS_CMD:
					dataOut.writeByte(StreamHeaders.SEND_PILOT_PROPS_CMD);
					dataOut.writeFloat(pilotModel.getWheelDiameter());
					dataOut.writeFloat(pilotModel.getTrackWidth());
					dataOut.writeChar(pilotModel.getMotorLeft());
					dataOut.writeChar(pilotModel.getMotorRight());
					dataOut.flush();
					break;
					
				case StreamHeaders.STOP_PID_CMD:
					dataOut.writeByte(StreamHeaders.STOP_PID_CMD);
					dataOut.flush();
					break;
					
				default:
					throw new IllegalArgumentException("Invalid command type. Please use StreamHeaders class constants.");
			}
		} catch (IOException ex) {
			System.out.println("Failed to send command " + commandType);
			ex.printStackTrace();
		} finally {
			try {
				dataOut.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public void sendPilotParam(byte paramType) {
		if (!connected) {
			System.out.println("Not connected, cannot send param " + paramType);
			return;
		}
		DataOutputStream dataOut = new DataOutputStream(conn.getOutputStream());
		try {
			dataOut.writeByte(StreamHeaders.SEND_SINGLE_PILOT_PARAM_CMD); // Write header stating packet contains single param
			dataOut.writeByte(paramType); // Write byte stating parameter type
			
			switch(paramType) {
				case StreamHeaders.PILOT_PARAM_SPEED:
					dataOut.writeInt(pilotModel.getSpeed());  // Write param
					break;
				case StreamHeaders.PILOT_PARAM_ACCELERATION:
					dataOut.writeInt(pilotModel.getAcceleration());  // Write param
					break;
				case StreamHeaders.PILOT_PARAM_ROTATESPEED:
					dataOut.writeInt(pilotModel.getRotateSpeed());  // Write param
					break;
				case StreamHeaders.PILOT_PARAM_WHEELDIAMETER:
					dataOut.writeFloat(pilotModel.getWheelDiameter());  // Write param
					break;
				case StreamHeaders.PILOT_PARAM_TRACKWIDTH:
					dataOut.writeFloat(pilotModel.getTrackWidth());  // Write param
					break;
			default:
				throw new IllegalArgumentException("Invalid param type. Please use constants from StreamHeaders class.");
			}
		} catch (IOException ex) {
			System.out.println("Failed to send single param. Type: " + paramType);
			ex.printStackTrace();
		} finally {
			try {
				dataOut.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public float getDeadband() {
		return pidModel.getDeadband();
	}

	public void setDeadband(float deadband) {
		pidModel.setDeadband(deadband);
	}

	public float getRampExponent() {
		return pidModel.getRampExponent();
	}

	public void setRampExponent(float rampExponent) {
		pidModel.setRampExponent(rampExponent);
	}

	public float getRampThreshold() {
		return pidModel.getRampThreshold();
	}

	public void setRampThreshold(float rampThreshold) {
		pidModel.setRampThreshold(rampThreshold);
	}

	public float getKp() {
		return pidModel.getKp();
	}

	public void setKp(float kp) {
		pidModel.setKp(kp);
	}

	public float getKi() {
		return pidModel.getKi();
	}

	public void setKi(float ki) {
		pidModel.setKi(ki);
	}

	public float getKd() {
		return pidModel.getKd();
	}

	public void setKd(float kd) {
		pidModel.setKd(kd);
	}

	public float getLimitHigh() {
		return pidModel.getLimitHigh();
	}

	public void setLimitHigh(float limitHigh) {
		pidModel.setLimitHigh(limitHigh);
	}

	public float getLimitLow() {
		return pidModel.getLimitLow();
	}

	public void setLimitLow(float limitLow) {
		pidModel.setLimitLow(limitLow);
	}

	public float getIntegralLimitHigh() {
		return pidModel.getIntegralLimitHigh();
	}

	public void setIntegralLimitHigh(float integralLimitHigh) {
		pidModel.setIntegralLimitHigh(integralLimitHigh);
	}

	public float getIntegralLimitLow() {
		return pidModel.getIntegralLimitLow();
	}

	public void setIntegralLimitLow(float integralLimitLow) {
		pidModel.setIntegralLimitLow(integralLimitLow);
	}

	public int getDelay() {
		return pidModel.getDelay();
	}

	public void setDelay(int delay) {
		pidModel.setDelay(delay);
	}

	public int getPilotSpeed() {
		return pilotModel.getSpeed();
	}

	public void setPilotSpeed(int speed) {
		pilotModel.setSpeed(speed);
//		sendPilotParam(StreamHeaders.PILOT_PARAM_SPEED);
	}

	public int getPilotAcceleration() {
		return pilotModel.getAcceleration();
	}

	public void setPilotAcceleration(int acceleration) {
		pilotModel.setAcceleration(acceleration);
	}

	public int getPilotRotateSpeed() {
		return pilotModel.getRotateSpeed();
	}

	public void setPilotRotateSpeed(int rotateSpeed) {
		pilotModel.setRotateSpeed(rotateSpeed);
	}

	public float getPilotTrackWidth() {
		return pilotModel.getTrackWidth();
	}

	public void setPilotTrackWidth(float trackWidth) {
		pilotModel.setTrackWidth(trackWidth);
	}

	public float getPilotWheelDiameter() {
		return pilotModel.getWheelDiameter();
	}

	public void setPilotWheelDiameter(float wheelDiameter) {
		pilotModel.setWheelDiameter(wheelDiameter);
	}

	public char getMotorLeft() {
		return pilotModel.getMotorLeft();
	}

	public void setMotorLeft(char motorLeft) {
		pilotModel.setMotorLeft(motorLeft);
	}

	public char getMotorRight() {
		return pilotModel.getMotorRight();
	}

	public void setMotorRight(char motorRight) {
		pilotModel.setMotorRight(motorRight);
	}

	public void setMotorSpeeds(int leftMotorSpeed, int rightMotorSpeed) {
		if (robotStateView != null) {
			robotStateView.setMotorSpeeds(leftMotorSpeed, rightMotorSpeed);
		}
	}

	public void setMV(int MV) {
		if (robotStateView != null) {
			robotStateView.setMV(MV);
		}
	}

	public void setLightValue(int lightValue) {
		if (robotStateView != null) {
			robotStateView.setLightValue(lightValue);
		}
	}
}


