package PIDConfigurator;

import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.border.EmptyBorder;
import java.awt.Color;
import javax.swing.border.EtchedBorder;
import javax.swing.JButton;
import java.awt.Dimension;

public class RobotStatePanel extends JPanel {
	
	private SlidingBar leftMotorBar;
	private SlidingBar rightMotorBar;
	private JLabel lblColor;
	private JLabel lblRunning;
	private JLabel lblMv;
	private JLabel lblMV;
	private JLabel lblLightvalue;
	private JLabel lblLightValue;

	public RobotStatePanel() {
		super();
		setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		setLayout(new MigLayout("", "[:100:100px][][][][][][:100px:100px]", "[][][][][][][][][][][][]"));
		
		JLabel lblRobotState = new JLabel("Robot State");
		lblRobotState.setFont(new Font("Tahoma", Font.BOLD, 16));
		add(lblRobotState, "flowx,cell 0 0 5 1,alignx left");
		
		JLabel lblLeftMotor = new JLabel("Left Motor");
		lblLeftMotor.setFont(new Font("Tahoma", Font.BOLD, 12));
		add(lblLeftMotor, "cell 0 2,alignx center");
		
		JLabel lblRightMotor = new JLabel("Right motor");
		lblRightMotor.setFont(new Font("Tahoma", Font.BOLD, 12));
		add(lblRightMotor, "cell 6 2,alignx center");
		
		leftMotorBar = new SlidingBar();
		add(leftMotorBar, "cell 0 3 1 9,grow");
		
		rightMotorBar = new SlidingBar();
		add(rightMotorBar, "cell 6 3 1 9,grow");
		
		lblRunning = new JLabel("Stopped");
		add(lblRunning, "flowx,cell 6 0,alignx center,aligny center");
		
		lblColor = new JLabel("");
		lblColor.setOpaque(true);
		lblColor.setBackground(Color.RED);
		lblColor.setBorder(new EmptyBorder(5, 5, 5, 5));
		add(lblColor, "cell 6 0,alignx left");
		
		lblMv = new JLabel("MV:");
		lblMv.setFont(new Font("Tahoma", Font.BOLD, 11));
		add(lblMv, "cell 2 4,alignx center");
		
		lblMV = new JLabel("");
		lblMV.setPreferredSize(new Dimension(50, 14));
		lblMV.setMaximumSize(new Dimension(50, 14));
		lblMV.setMinimumSize(new Dimension(50, 14));
		add(lblMV, "cell 3 4 2 1,aligny center");
		
		lblLightvalue = new JLabel("Light:");
		lblLightvalue.setFont(new Font("Tahoma", Font.BOLD, 11));
		add(lblLightvalue, "cell 2 5");
		
		lblLightValue = new JLabel("");
		lblLightValue.setPreferredSize(new Dimension(50, 14));
		lblLightValue.setMaximumSize(new Dimension(50, 14));
		lblLightValue.setMinimumSize(new Dimension(50, 14));
		add(lblLightValue, "cell 3 5 2 1");
	}
	
	public void setMotorSpeeds(int leftSpeed, int rightSpeed) {
		leftMotorBar.setValue(leftSpeed);
		rightMotorBar.setValue(rightSpeed);
	}
	
	public void setRunning(Boolean isRunning) {
		if (isRunning) {
			lblColor.setBackground(Color.green);
			lblRunning.setText("Running");
		} else {
			lblColor.setBackground(Color.red);
			lblRunning.setText("Stopped");
		}
	}
	
	public void setMaxSpeed(int maxSpeed) {
		leftMotorBar.setMinValue(-maxSpeed);
		leftMotorBar.setMaxValue(maxSpeed);
		rightMotorBar.setMinValue(-maxSpeed);
		rightMotorBar.setMaxValue(maxSpeed);
	}

	public void setLightValue(int lightValue) {
		lblLightValue.setText(String.valueOf(lightValue));
	}

	public void setMV(int MV) {
		lblMV.setText(String.valueOf(MV));		
	}
}
