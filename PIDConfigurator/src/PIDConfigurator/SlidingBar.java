package PIDConfigurator;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.font.FontRenderContext;
import java.awt.geom.Rectangle2D;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class SlidingBar extends JPanel {
	
	private int value = 0; 
	private int _minValue = -100;
	private int _maxValue = 100;
	
	private String _maxLabel;
	private String _minLabel;
	
	private static int HORIZONTAL_PADDING = 20;
	private static int VERTICAL_PADDING = 20;
	
	
//	public static void main(String[] args) {
//		JFrame f = new JFrame();
//		f.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
//		f.setLocationRelativeTo(null);
//		
//		JPanel p = new JPanel();
//		p.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
//		
//		final SlidingBar bar = new SlidingBar();
////		bar.setPreferredSize(new Dimension(50,200));
////		bar.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
//		p.add(bar);
//		JSlider slider = new JSlider(-100,100);
//		slider.addChangeListener(new ChangeListener() {
//
//			@Override
//			public void stateChanged(ChangeEvent arg0) {
//				bar.setValue(((JSlider)arg0.getSource()).getValue());
//			}
//			
//		});
//		
//		p.add(slider);
//		
//		f.setContentPane(p);
//		f.pack();
//		f.setVisible(true);
//	}
	
	public SlidingBar() {
		super();
		this.setMinimumSize(new Dimension(HORIZONTAL_PADDING*2+15,VERTICAL_PADDING*2+100));
	};
	
	public SlidingBar(int minValue, int maxValue) {
		this._minValue = minValue;
		this._maxValue = maxValue;
	}
	
	public String getMaxLabel() {
		return _maxLabel != null ? _maxLabel : String.valueOf(_maxValue);
	}

	public void setMaxLabel(String _maxLabel) {
		this._maxLabel = _maxLabel;
	}

	public String getMinLabel() {
		return _minLabel != null ? _minLabel : String.valueOf(_minValue);
	}

	public void setMinLabel(String _minLabel) {
		this._minLabel = _minLabel;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value > _maxValue ? _maxValue : (value < _minValue ? _minValue : value);
		this.repaint();
	}

	public int getMinValue() {
		return _minValue;
	}

	public void setMinValue(int minValue) {
		this._minValue = minValue;
	}

	public int getMaxValue() {
		return _maxValue;
	}

	public void setMaxValue(int maxValue) {
		this._maxValue = maxValue;
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D)g;
		
		Rectangle bounds = this.getBounds();
		
		int barHeight = bounds.height-VERTICAL_PADDING*2;
		int barWidth = bounds.width - HORIZONTAL_PADDING*2;
		
		g2d.translate(HORIZONTAL_PADDING, VERTICAL_PADDING);
		
		// Max value header
		centerString(g2d,
				new Rectangle(0,-20,barWidth,20),
				getMaxLabel(),
				g2d.getFont().deriveFont(Font.BOLD)
				);
		
		// Min value header
		centerString(g2d,
				new Rectangle(0,barHeight,barWidth,20),
				getMinLabel(),
				g2d.getFont().deriveFont(Font.BOLD)
				);

		double valuePercentage = (double)(value-_minValue) / (_maxValue - _minValue);
		
//		int red = (int) (valuePercentage > 0.5 ? 0 : (1-valuePercentage)*255);
//		int green = (int) (valuePercentage < 0.5 ? 0 : valuePercentage*255);
		
		int red = (int)((1-valuePercentage)*255);
		int green = (int)((valuePercentage)*200 +55);
		Color barColor = new Color(red, green, 0);
		g2d.setColor(barColor);
		g2d.fill(new Rectangle(0,(int)Math.ceil((barHeight*(1-valuePercentage))), barWidth,(int) (barHeight*valuePercentage)));		
		
		g2d.setColor(Color.DARK_GRAY);
		g2d.draw(new Rectangle(0,0,barWidth,barHeight));
		
		g2d.setColor(Color.BLACK);
		g2d.setStroke(new BasicStroke(2f));
		g2d.drawLine(-10, barHeight/2, barWidth+10, barHeight/2);
		
		g2d.translate(-HORIZONTAL_PADDING, -VERTICAL_PADDING);
		this.paintBorder(g);
	}
	
	private void centerString(Graphics g, Rectangle r, String s, Font font) {
	    FontRenderContext frc = 
	            new FontRenderContext(null, true, true);

	    Rectangle2D r2D = font.getStringBounds(s, frc);
	    int rWidth = (int) Math.round(r2D.getWidth());
	    int rHeight = (int) Math.round(r2D.getHeight());
	    int rX = (int) Math.round(r2D.getX());
	    int rY = (int) Math.round(r2D.getY());

	    int a = (r.width / 2) - (rWidth / 2) - rX;
	    int b = (r.height / 2) - (rHeight / 2) - rY;

	    g.setFont(font);
	    g.drawString(s, r.x + a, r.y + b);
	}
	
}
