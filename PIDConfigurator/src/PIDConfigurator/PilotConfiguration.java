package PIDConfigurator;

public class PilotConfiguration {
		
	private int speed = 100;
	private int acceleration = 600;
	private int rotateSpeed = 600;
	
	private float trackWidth = 105f;
	private float wheelDiameter = 43.2f;
	private char motorLeft = 'C';
	private char motorRight = 'A';

	public char getMotorLeft() {
		return motorLeft;
	}
	public void setMotorLeft(char motorLeft) {
		this.motorLeft = motorLeft;
	}
	public char getMotorRight() {
		return motorRight;
	}
	public void setMotorRight(char motorRight) {
		this.motorRight = motorRight;
	}
	public float getTrackWidth() {
		return trackWidth;
	}
	public void setTrackWidth(float trackWidth) {
		this.trackWidth = trackWidth;
	}
	public float getWheelDiameter() {
		return wheelDiameter;
	}
	public void setWheelDiameter(float wheelDiameter) {
		this.wheelDiameter = wheelDiameter;
	}
	public int getSpeed() {
		return speed;
	}
	public void setSpeed(int speed) {
		this.speed = speed;
	}
	public int getAcceleration() {
		return acceleration;
	}
	public void setAcceleration(int acceleration) {
		this.acceleration = acceleration;
	}
	public int getRotateSpeed() {
		return rotateSpeed;
	}
	public void setRotateSpeed(int rotateSpeed) {
		this.rotateSpeed = rotateSpeed;
	}	
}
