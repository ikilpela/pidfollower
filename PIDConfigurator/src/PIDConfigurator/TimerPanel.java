package PIDConfigurator;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.SwingConstants;
import javax.swing.Timer;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JFrame;

public class TimerPanel extends JPanel implements ActionListener {
	
	private static SimpleDateFormat format = new SimpleDateFormat("mm : ss : SSS");
	
	private Timer timer;
	private long startTime = -1;
	private long stopTime = -1;
	
	private JLabel lblTime;
	
	private Calendar time = Calendar.getInstance();

	private JButton btnStart;

	private JButton btnStop;

	private JPanel btnPanel;
	
	public static void main(String[] args) {
		JFrame f = new JFrame();
		f.getContentPane().add(new TimerPanel());
		f.setLocationRelativeTo(null);
		f.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		f.pack();
		f.setVisible(true);
	}
	
	public TimerPanel() {
		timer = new Timer(10,this);
		setBorder(new EmptyBorder(5, 5, 5, 5));
		setLayout(new BorderLayout(0, 0));
		
		lblTime = new JLabel("0 : 00 : 000");
		lblTime.setHorizontalAlignment(SwingConstants.CENTER);
		lblTime.setFont(new Font("Sylfaen", Font.PLAIN, 18));
		add(lblTime, BorderLayout.CENTER);
		
		btnPanel = new JPanel();
		add(btnPanel, BorderLayout.SOUTH);
		
		btnStart = new JButton("Start");
		btnStart.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				start();
			}
		});
		
		btnPanel.add(btnStart);
		
		btnStop = new JButton("Pause");
		btnStop.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				stop();
			}
		});
		btnPanel.add(btnStop);
	}
	
	public TimerPanel(Boolean hideButtons) {
		this();
		btnPanel.setVisible(!hideButtons);
	}
	
	public Font getTimerFont() {
		return lblTime.getFont();
	}
	
	public void setTimerFont(Font font) {
		lblTime.setFont(font);
	}

	public static SimpleDateFormat getFormat() {
		return format;
	}

	public static void setFormat(SimpleDateFormat format) {
		TimerPanel.format = format;
	}

	public boolean isRunning() {
		return timer.isRunning();
	}
	
	public Calendar getTime() {
		return time;
	}

	public void start() {
		System.out.println("Timer start called");
		if (startTime < 0) {
			startTime = System.currentTimeMillis();
		} else if (!timer.isRunning()) {
			startTime += (System.currentTimeMillis() - stopTime);
		}
		if (!timer.isRunning()) {
			timer.start();
			btnStart.setEnabled(false);
			btnStop.setText("Pause");
		}
	}
	
	public void stop() {
		if (timer.isRunning()) {
			stopTime = System.currentTimeMillis();
			timer.stop();
			btnStop.setText("Reset");
		} else {
			this.reset();
			btnStop.setText("Stop");
		}
		btnStart.setEnabled(true);	
	}

	public void reset() {
		if (this.isRunning()) {
			startTime = System.currentTimeMillis();
		} else {
			startTime = -1;
			lblTime.setText("0 : 00 : 000");
		}
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (timer.isRunning()) {
			time.setTimeInMillis(System.currentTimeMillis()-startTime);
			lblTime.setText(format.format(time.getTime()));
		}
	}
}
