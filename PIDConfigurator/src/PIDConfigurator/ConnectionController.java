package PIDConfigurator;

public interface ConnectionController {
	public void connect();
	public void connect(long timeout);
	public void disconnect();
}
