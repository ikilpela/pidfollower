package PIDConfigurator;

import java.awt.BorderLayout;
import java.awt.Font;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.EmptyBorder;
import java.awt.Component;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EtchedBorder;
import java.awt.Dimension;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Main {

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {				
				JFrame f = new JFrame();
				f.setPreferredSize(new Dimension(800, 800));
				f.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				f.setTitle("PIDFollower configuration tool");
				PIDConfiguration pidModel = new PIDConfiguration();
				PilotConfiguration pilotModel = new PilotConfiguration();
				
				ConnectionStatusView csv = new ConnectionStatusView();
				csv.setAlignmentY(Component.CENTER_ALIGNMENT);
				
				JPanel mainPanel = new JPanel();
				mainPanel.setLayout(new BorderLayout());
				
				JPanel topPanel = new JPanel();
				topPanel.setBorder(new EmptyBorder(10, 5, 10, 5));
				topPanel.setLayout(new BorderLayout(0, 0));
				topPanel.add(csv, BorderLayout.EAST);
				
				mainPanel.add(topPanel, BorderLayout.NORTH);
				
				JSplitPane splitPane = new JSplitPane();
				splitPane.setResizeWeight(1.0);
				splitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);
				splitPane.setContinuousLayout(true);
				
				mainPanel.add(splitPane,BorderLayout.CENTER);
				
				JSplitPane splitPane_1 = new JSplitPane();
				splitPane_1.setContinuousLayout(true);
				splitPane.setLeftComponent(splitPane_1);
				
				PIDConfigUI configUI = new PIDConfigUI();
				configUI.setBorder(new CompoundBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), new EmptyBorder(5, 5, 5, 5)));
				splitPane_1.setLeftComponent(configUI);
				
				PilotConfigUI pilotConfigUI = new PilotConfigUI();
				pilotConfigUI.setBorder(new CompoundBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), new EmptyBorder(5, 5, 5, 5)));
				splitPane_1.setRightComponent(pilotConfigUI);
				
				f.setContentPane(mainPanel);
				
				f.pack();
				f.setLocationRelativeTo(null);
				f.setVisible(true);
				
				// Initalize controller
				final Controller controller = new Controller(pidModel, pilotModel, configUI, pilotConfigUI, csv);
				
				JSplitPane splitPane_2 = new JSplitPane();
				splitPane_2.setContinuousLayout(true);
				splitPane_2.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
				splitPane.setRightComponent(splitPane_2);
				
				RobotStatePanel robotStatePanel = new RobotStatePanel();
				robotStatePanel.setBorder(new CompoundBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), new EmptyBorder(0, 5, 5, 5)));
				splitPane_2.setLeftComponent(robotStatePanel);
				
				TimerPanel timerPanel = new TimerPanel();
				timerPanel.setBorder(new CompoundBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), new EmptyBorder(5, 5, 5, 5)));
				timerPanel.setTimerFont(timerPanel.getTimerFont().deriveFont(40f));
				splitPane_2.setRightComponent(timerPanel);
				
				JLabel lblPidfollowerConfigurationTool = new JLabel("PIDFollower utilities");
				lblPidfollowerConfigurationTool.setFont(new Font("Tahoma", Font.BOLD, 18));
				topPanel.add(lblPidfollowerConfigurationTool, BorderLayout.WEST);
				
//				JButton btnTest = new JButton("Test");
//				btnTest.addActionListener(new ActionListener() {
//					boolean running = false;
//					public void actionPerformed(ActionEvent arg0) {
//						running = !running;
//						controller.setRunning(running);
//					}
//				});
//				topPanel.add(btnTest, BorderLayout.CENTER);
				
				// Register controller to views
				configUI.registerController(controller);
				pilotConfigUI.registerController(controller);
				csv.registerController(controller);
				
				// Register timerPanel to controller
				controller.setTimerPanel(timerPanel);
				controller.setRobotStateView(robotStatePanel);
			}
		});
	}

}
