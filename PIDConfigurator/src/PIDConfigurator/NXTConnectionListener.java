package PIDConfigurator;

import java.io.DataInputStream;
import java.io.IOException;
import java.util.Objects;

import lejos.pc.comm.NXTConnector;

public class NXTConnectionListener implements Runnable {
	
	private DataInputStream dataIn;
	private Controller controller;
	private volatile boolean closed = false;

	public NXTConnectionListener(NXTConnector conn, Controller controller) {
		Objects.requireNonNull(conn, "NXTConnector must not be null");
		Objects.requireNonNull(controller, "Controller must not be null");
		this.controller = controller;
		this.dataIn = new DataInputStream(conn.getInputStream());
	}
	
	public void close() {
		this.closed = true;
	}

	@Override
	public void run() {
		try {
			while (!closed) {
				int cmd = dataIn.readByte();
//				System.out.println("Read cmd " + cmd);
				switch (cmd) {
				case StreamHeaders.NXT_POLL_PID_CMD: // Return call from "Poll NXT"
					int delay = dataIn.readInt();
					float kp = dataIn.readFloat();
					float kd = dataIn.readFloat();
					float ki = dataIn.readFloat();
					
					float limitHigh = dataIn.readFloat();
					float limitLow = dataIn.readFloat();
					float integralLimitHigh = dataIn.readFloat();
					float integralLimitLow = dataIn.readFloat();
	
					float deadband = dataIn.readFloat();
					float rampExponent = dataIn.readFloat();
					float rampThreshold = dataIn.readFloat();
					
					controller.setDelay((int)delay);
					controller.setKp(kp);
					controller.setKd(kd);
					controller.setKi(ki);
					controller.setLimitHigh(limitHigh);
					controller.setLimitLow(limitLow);
					controller.setIntegralLimitHigh(integralLimitHigh);
					controller.setIntegralLimitLow(integralLimitLow);
					controller.setDeadband(deadband);
					controller.setRampExponent(rampExponent);
					controller.setRampThreshold(rampThreshold);
	
					controller.updatePIDView();
					break;
				case StreamHeaders.NXT_POLL_PILOT_CMD:
					int speed = dataIn.readInt();
					int acceleration = dataIn.readInt();
					int rotateSpeed = dataIn.readInt();
					float wheelDiameter = dataIn.readFloat();
					float trackWidth = dataIn.readFloat();
					char leftMotor = dataIn.readChar();
					char rightMotor = dataIn.readChar();
					controller.setPilotSpeed(speed);
					controller.setPilotAcceleration(acceleration);
					controller.setPilotRotateSpeed(rotateSpeed);
					controller.setPilotWheelDiameter(wheelDiameter);
					controller.setPilotTrackWidth(trackWidth);
					controller.setMotorLeft(leftMotor);
					controller.setMotorRight(rightMotor);
					controller.updatePilotView();
					break;
				case StreamHeaders.SEND_FOLLOWER_STATE_CMD:
					int leftMotorSpeed = dataIn.readInt();
					int rightMotorSpeed = dataIn.readInt();
					int MV = dataIn.readInt();
					int lightValue = dataIn.readInt();
					controller.setMotorSpeeds(leftMotorSpeed, rightMotorSpeed);
					controller.setMV(MV);
					controller.setLightValue(lightValue);
					break;
				case StreamHeaders.SEND_FOLLOWER_STARTED_CMD:
					controller.setRunning(true);
					System.out.println("Robot started!");
					break;
				case StreamHeaders.SEND_FOLLOWER_STOPPED_CMD:
					controller.setRunning(false);
					System.out.println("Robot stopped!");
					break;
				default:
					System.out.println("Unrecognized header byte " + cmd);
				}
			}
		} catch (IOException ex) {
			System.out.println("NXTConnectionListener failed");
			ex.printStackTrace();
		} finally {
			try {
				dataIn.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("NXTConnectionListener closed");
	}
}
