package PIDConfigurator;

import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Dimension;
import javax.swing.BoxLayout;
import java.awt.Component;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ConnectionStatusView extends JPanel {

	public enum ConnectionStatus {
		CONNECTED,
		CONNECTING,
		DISCONNECTED
	};
	
	private boolean animating = false;
	private ConnectionController controller;
	
	private ConnectionStatus connectionStatus = ConnectionStatus.DISCONNECTED;
	private JLabel lblColor;
	private JLabel lblString;
	private JButton btnConnect;
	
	public ConnectionStatusView() {
		setBorder(new EmptyBorder(0, 5, 0, 5));
		setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		
		lblColor = new JLabel("");
		lblColor.setOpaque(true);
		lblColor.setBorder(new EmptyBorder(5, 5, 5, 5));
		lblColor.setBackground(Color.RED);
		add(lblColor);
		
		lblString = new JLabel("Not connected");
		lblString.setMaximumSize(new Dimension(120, 14));
		lblString.setPreferredSize(new Dimension(120, 14));
		lblString.setMinimumSize(new Dimension(120, 14));
		lblString.setBorder(new EmptyBorder(0, 10, 0, 10));
		lblString.setAlignmentX(Component.CENTER_ALIGNMENT);
		
		add(lblString);
		
		btnConnect = new JButton("Connect");
		btnConnect.setMaximumSize(new Dimension(120, 23));
		btnConnect.setPreferredSize(new Dimension(120, 23));
		btnConnect.setMinimumSize(new Dimension(120, 23));
		btnConnect.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (controller == null) {
					return;
				}
				switch (ConnectionStatusView.this.getConnectionStatus()) {
					case CONNECTED:
						if (controller != null) {
							controller.disconnect();
						}
						break;
					case CONNECTING:
						break;
					case DISCONNECTED:
						if (controller != null) {
							controller.connect(3000);
							setConnectionStatus(ConnectionStatus.CONNECTING);
						} 
						break;
					default:
						break;					
				}
			}
		});
		add(btnConnect);
	}

	public ConnectionStatus getConnectionStatus() {
		return connectionStatus;
	}
	
	public void registerController(ConnectionController controller) {
		this.controller = controller;
	}

	public void setConnectionStatus(ConnectionStatus connectionStatus, String text) {
		this.connectionStatus = connectionStatus;
		switch (connectionStatus) {
		case CONNECTED:
			lblColor.setBackground(Color.GREEN);
			animating = false;
			lblString.setText("Connected");
			btnConnect.setText("Disconnect");
			btnConnect.setEnabled(true);
			break;
		case CONNECTING:
			lblColor.setBackground(Color.YELLOW);
			animating = true;
			btnConnect.setEnabled(false);
			Thread t = new Thread(new Runnable() {

				@Override
				public void run() {
					int i = 0;
					while (animating) {
						String text = "Connecting";
						for (int j = 0; j <= (i+1)%3; j++) {
							text += ".";
						}
						lblString.setText(text);
						i++;
						try {
							Thread.sleep(500);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
			});
			t.start();		
			break;
		case DISCONNECTED:
			lblColor.setBackground(Color.RED);
			animating = false;
			lblString.setText("Not connected");
			btnConnect.setText("Connet");
			btnConnect.setEnabled(true);
			break;
		}
		if (text != "") {
			lblString.setText(text);
		}
	}
	public void setConnectionStatus(ConnectionStatus status) {
		setConnectionStatus(status,"");
	}
}
