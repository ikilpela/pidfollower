# PIDFollower - A Lego NXT LineFollower robot with remote GUI

A school project. 

The goal was to make a simple line following robot using Java with LeJOS framework and an accompanying GUI that can be used to transmit values to the robot. I was acting as the project leader and had no programming duties apart from helping my team with any problems they couldn't solve themselves so I made my own implementation during my free time. 
 
The line follower implementation uses a single PID Controller to control the steering of the robot. 
 
The PIDConfigurator program runs on a computer and can be used to monitor the motor speeds of the robot in real time and change different parameters via Bluetooth. GUI also has a timer that is automatically started when the robot starts a run and stopped when the run ends.