package pidfollower;

import java.io.IOException;

import org.lejos.util.LogMessageManager;
import org.lejos.util.NXTDataLogger;
import org.lejos.util.PIDTuner;
import org.lejos.util.PIDTuningProvider;
import org.lejos.util.PIDController;

import lejos.nxt.Button;
import lejos.nxt.LCD;
import lejos.nxt.LightSensor;
import lejos.nxt.SensorPort;
import lejos.nxt.Sound;
import lejos.nxt.UltrasonicSensor;
import lejos.nxt.comm.BTConnection;
import lejos.nxt.comm.Bluetooth;
import lejos.nxt.comm.NXTConnection;
import lejos.robotics.RegulatedMotor;
import lejos.robotics.navigation.DifferentialPilot;
import lejos.util.PilotProps;
import lightsensor.Calibrator;
import linefollower.Utils;

/**
 * A Lego NXT line follower class which uses PIDController and Differential Pilot to follow a line. 
 * 
 * @author Ismo
 *
 */
public class PIDFollower {

	private final DifferentialPilot pilot;
	private final LightSensor light;
	
	private PIDController pid;
	private CustomPIDTuner myPIDTuner;
	private BTConnectionManager btManager;

	private volatile boolean running = false;
	private volatile boolean scanForObstacles = false;
	
	private float obstacleDistance = 255;
	
	// Create separate thread for scanning for obstacles
	private Thread ultraThread = new Thread(new Runnable() {

		private final UltrasonicSensor ultraSonic = new UltrasonicSensor(SensorPort.S3);
		
		@Override
		public void run() {
			
			while (scanForObstacles) {				
				obstacleDistance = ultraSonic.getRange();
				LCD.drawString("Dist:" + obstacleDistance, 4, 4);
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
	});
	private int obstacleCount = 0;
	private int maxObstacles = 1;

	// Some constants
	private static final String PID_SETTINGS_FILENAME = "pidparams.cfg";
	private static final int TARGET_LIGHT_VALUE = 50;
	private static final double PILOT_SPEED = 125;
	

	/**
	 * Sets the PIDController parameters from persistent file if available.
	 * If file is not available, uses default values. 
	 */
	private void setDefaultPIDParams() {
		if (!myPIDTuner.loadFromFile(PID_SETTINGS_FILENAME)) {
			myPIDTuner.setDelay(10);
			myPIDTuner.setSP(TARGET_LIGHT_VALUE);
			myPIDTuner.setKp(2.4f);
			myPIDTuner.setKi(0.00025f);
			myPIDTuner.setKd(28f);
			myPIDTuner.setMVDeadband(5);
			myPIDTuner.setRampExponent(2f);
			myPIDTuner.setRampTriggerThreshold(100);
			myPIDTuner.setMVHighLimit(200);
			myPIDTuner.setMVLowLimit(-200);
			myPIDTuner.setIntegralWindupHighLimit(50);
			myPIDTuner.setIntegralWindupLowLimit(-50);
		}
	}
	
	/**
	 * Main method. Initializes LightSensor and a DifferentialPilot and then calibrates the LightSensor. 
	 * Then uses these to create a new PIDFollower and starts it.
	 * @param args
	 */
	public static void main(String[] args) {
		LightSensor light = new LightSensor(SensorPort.S1);
		DifferentialPilot pilot = createPilot();

		Calibrator.calibrate(light);
		PIDFollower pf = new PIDFollower(pilot, light);
		pf.start();
	}
	
	/**
	 * Constructor. Creates a PIDFollower instance with provided Pilot and Light instance.
	 * @param pilot DifferentialPilot to be used
	 * @param light LightSensor to be used
	 */
	public PIDFollower(DifferentialPilot pilot, LightSensor light) {
		this.pilot = pilot;
		this.light = light;
		pid = new PIDController(TARGET_LIGHT_VALUE, 10);
		myPIDTuner = new CustomPIDTuner(pid);
		setDefaultPIDParams();
	}
	
	/**
	 * Starts the PIDFollower. First prompts use to choose communication method (PIDConfigurator or NXJCharting logger)
	 * and then starts line following loop. 
	 */
	public void start() {
		
		// Choose communication method
		LCD.clear();
		LCD.drawString("Choose comms:", 0, 0);
		LCD.drawString("L = OMA",0,1);
		LCD.drawString("R = Charting",0,2);
		switch (Button.waitForAnyPress()) {
			case Button.ID_RIGHT: 
				enableDataLogger();
				break;
			case Button.ID_LEFT:
				enableBTListener();
				break;
			default:
				break;
		} 
		
		// Prompt keypress before starting the linefollower
		LCD.clear();
		LCD.drawString("Press ESC to",0,0);
		LCD.drawString("shutdown or", 0, 1);
		LCD.drawString("any other key", 0, 2);
		LCD.drawString("to start PID", 0, 3);
		
		// Outer loop that can be repeated after stopping the follower
		while (Button.waitForAnyPress() != Button.ID_ESCAPE) {
			
			// Initialize start countdown
			Utils.countdown(3);
			
			// Start pilot
			pilot.forward();
			running = true;
			
			// If BTManager is enabled, notify PC app that run started
			if (btManager != null) {
				btManager.sendFollowerStarted();
			}
			
			// Start ultrasonic scanning for obstacles
			scanForObstacles = true;
			ultraThread.start();
			
			// Start the inner loop that does the actual line following
			runPID(); 
			
			// Stop ultrasonic thread by changing scanForObstacles to false
			scanForObstacles = false;
			
			// If BTManager is enabled, notify PC app that run stopped
			if (btManager != null) {
				btManager.sendFollowerStopped();
			}
			
			// After run is complete, prompt user if he wants to save settings
			LCD.clear();
			LCD.drawString("Press enter", 0, 0);
			LCD.drawString("to save cfg",0,1);
			LCD.drawString("or any other key", 0, 2);
			LCD.drawString("to continue",0,3);
			if (Button.waitForAnyPress() == Button.ID_ENTER) {
				myPIDTuner.saveToFile(PID_SETTINGS_FILENAME);
			}
			
			// Ask user if he wants to restart PID loop or quit.
			LCD.clear();
			LCD.drawString("Press ESC to",0,0);
			LCD.drawString("shutdown or", 0, 1);
			LCD.drawString("any other key", 0, 2);
			LCD.drawString("to restart PID", 0, 3);
		}
	}

	/**
	 * Line following loop
	 */
	private void runPID() {
		LCD.clear();
		LCD.drawString("Esc to quit",0,7);
		while (running && !Button.ESCAPE.isDown()) {
			int lightValue = light.readValue(); // Get light value from sensor
			int MV = pid.doPID(lightValue);  // Get steer value from PIDController
			LCD.drawString("MV: ", 0, 0);
			LCD.drawInt(MV, 4, 5, 0);
			pilot.steer(MV); // Steer according to value received from PIDController
			// If BTManager is enabled, send the state to PC application.
			if (btManager != null) {
				btManager.sendPIDFollowerState(lightValue,MV);
			}
			if (obstacleDistance < 15) {
				if (obstacleCount < maxObstacles) {
					avoidObstacle(obstacleDistance);
				} else {
					pilot.travel(obstacleDistance*10-3);
					this.running = false;
				}
			}
		}
		pilot.stop();
		
		if (Button.ESCAPE.isDown()) {
			// Short sleep to prevent ESCAPE keypress from quitting the outer loop
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Runs obstacle avoidance behaviour until line is reached again and then stops.
	 * @param distance Distance to the obstacle in units used for pilot parameters
	 */
	private void avoidObstacle(float distance) {
		pilot.stop();
		
		// Set rotate speed to low value to avoid losing traction when turning
		pilot.setRotateSpeed(100);
		// Rotate pilot 75 degrees right
		pilot.rotate(-75);
		// Start moving in a circular arc around the obstacle until line is reached again
		pilot.arc(distance*10*1.5, 180, true);
		// Wait until line is reached again
		while(light.getLightValue() < TARGET_LIGHT_VALUE) {
//			LCD.drawString("Looking!", 0, 0);
		}
		pilot.rotate(-50);
		pilot.stop();
		pilot.setRotateSpeed(pilot.getMaxRotateSpeed());
	}

	/**
	 * Helper method that initalizes the pilot from PilotProps persistent values or hardcoded
	 * values (WheelDiameter = 56, trackwidth = 105, leftmotor = C, rightmotor = A
	 * 
	 * @return DifferentialPilot instance with parameters loaded from PilotProps persistent values
	 */
	private static DifferentialPilot createPilot() {
		
		// Init PilotProps and try to load persistent values
		PilotProps pp = new PilotProps();
		try {		
			pp.loadPersistentValues();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// Init pilot constructor variables from persistent values or defaults
		float wheelDiameter = Float.valueOf(pp.getProperty(PilotProps.KEY_WHEELDIAMETER, "56"));
		float trackWidth = Float.valueOf(pp.getProperty(PilotProps.KEY_TRACKWIDTH, "105"));
		RegulatedMotor motorLeft = PilotProps.getMotor(pp.getProperty(PilotProps.KEY_LEFTMOTOR, "C"));
		RegulatedMotor motorRight = PilotProps.getMotor(pp.getProperty(PilotProps.KEY_RIGHTMOTOR, "A"));
		
		// Create the pilot
		DifferentialPilot pilot = new DifferentialPilot(wheelDiameter, trackWidth, motorLeft, motorRight);
		
		// Set speeds
		pilot.setTravelSpeed(PILOT_SPEED);
		pilot.setRotateSpeed(pilot.getMaxRotateSpeed());
		
		return pilot;
	}
	
	/**
	 * Enables the datalogger used by NXJChartinglogger tool
	 */
	private void enableDataLogger() {
		NXTDataLogger dlog = new NXTDataLogger();
		LCD.clear();
		LCD.drawString("Waiting BT...", 0, 0);
		NXTConnection conn = Bluetooth.waitForConnection(30000, NXTConnection.PACKET);
		LCD.clear();
		Sound.beep();
		try {
			dlog.startRealtimeLog(conn);
		} catch (Exception e) {
		}
		LogMessageManager lmm = LogMessageManager.getLogMessageManager(dlog);
		PIDTuner pidTuner = new PIDTuner(pid, lmm);
		pidTuner.setDisplayName("PIDFollower tuning");
	}
	
	/**
	 * Initializes the custom BTListener class coded for this class which will send and receive communications from
	 * PIDConfigurator app ran on PC
	 */
	private void enableBTListener() {
		LCD.clear();
		LCD.drawString("OmaBT selected", 0, 0);
		LCD.drawString("Waiting for BT",0,1);
		BTConnection conn = Bluetooth.waitForConnection(30000,NXTConnection.PACKET);
		btManager = new BTConnectionManager(conn, this);
		new Thread(btManager).start();
	}

	/**
	 * Stops the follower
	 */
	public void stop() {
		running = false;
	}

	/**
	 * Returns true if the PIDFollower is running
	 * @return True if the PIDFollower is running
	 */
	public boolean isRunning() {
		return running;
	}
	
	/**
	 * Returns the PIDTuningProvider which can be used to access PIDController parameters
	 * @return PIDTuningProvider implementation
	 */
	public PIDTuningProvider getTuningProvider() {
		return myPIDTuner;
	}

	/**
	 * Returns the Pilot used by this PIDFollower
	 * @return DifferentialPilot used by this PIDFollower
	 */
	public DifferentialPilot getPilot() {
		return pilot;
	}
}