package pidfollower;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.lejos.util.PIDController;
import org.lejos.util.PIDTuningProvider;

import lejos.nxt.Button;
import lejos.nxt.LCD;

/**
 * PIDTuningProvider implementation which allows easy access to PIDController parameters
 * and provides methods to persist parameters in a file on NXT flash memory. 
 * 
 * @author Ismo Kilpeläinen
 *
 */
public class CustomPIDTuner implements PIDTuningProvider {

	PIDController pid;
	
	/**
	 * Constructor. Creates new CustomPIDTuner to change and persist values of provided PIDController.
	 * @param pid PIDController to link with this tuner.
	 */
	public CustomPIDTuner(PIDController pid) {
		this.pid = pid;
	}
	
	@Override
	public float getKp() {
		return pid.getPIDParam(PIDController.PID_KP);
	}

	@Override
	public void setKp(float kp) {
		pid.setPIDParam(PIDController.PID_KP, kp);

	}

	@Override
	public float getKi() {

		return pid.getPIDParam(PIDController.PID_KI);
	}

	@Override
	public void setKi(float ki) {
		pid.setPIDParam(PIDController.PID_KI, ki);

	}

	@Override
	public float getKd() {
		return pid.getPIDParam(PIDController.PID_KD);
	}

	@Override
	public void setKd(float kd) {
		pid.setPIDParam(PIDController.PID_KD, kd);
	}

	@Override
	public float getRampExponent() {
		return pid.getPIDParam(PIDController.PID_RAMP_POWER);
	}

	@Override
	public void setRampExponent(float rampExponent) {
		pid.setPIDParam(PIDController.PID_RAMP_POWER, rampExponent);

	}

	@Override
	public float getRampTriggerThreshold() {
		return pid.getPIDParam(PIDController.PID_RAMP_THRESHOLD);
	}

	@Override
	public void setRampTriggerThreshold(float rampTriggerThreshold) {
		pid.setPIDParam(PIDController.PID_RAMP_THRESHOLD, rampTriggerThreshold);

	}

	@Override
	public float getMVDeadband() {
		return pid.getPIDParam(PIDController.PID_DEADBAND);
	}

	@Override
	public void setMVDeadband(float mVDeadband) {
		pid.setPIDParam(PIDController.PID_DEADBAND, mVDeadband);

	}

	@Override
	public float getMVHighLimit() {
		return pid.getPIDParam(PIDController.PID_LIMITHIGH);
	}

	@Override
	public void setMVHighLimit(float mVHighLimit) {
		pid.setPIDParam(PIDController.PID_LIMITHIGH, mVHighLimit);

	}

	@Override
	public float getMVLowLimit() {
		return pid.getPIDParam(PIDController.PID_LIMITLOW);
	}

	@Override
	public void setMVLowLimit(float mVLowLimit) {
		pid.setPIDParam(PIDController.PID_LIMITLOW, mVLowLimit);
	}

	@Override
	public float getIntegralWindupLowLimit() {
		return pid.getPIDParam(PIDController.PID_I_LIMITLOW);
	}

	@Override
	public void setIntegralWindupLowLimit(float integralWindupLowLimit) {
		pid.setPIDParam(PIDController.PID_I_LIMITLOW, integralWindupLowLimit);

	}

	@Override
	public float getIntegralWindupHighLimit() {
		return pid.getPIDParam(PIDController.PID_I_LIMITHIGH);
	}

	@Override
	public void setIntegralWindupHighLimit(float integralWindupHighLimit) {
		pid.setPIDParam(PIDController.PID_I_LIMITHIGH,integralWindupHighLimit);
	}

	@Override
	public float getSP() {
		return pid.getPIDParam(PIDController.PID_SETPOINT);
	}

	@Override
	public void setSP(float sP) {
		pid.setPIDParam(PIDController.PID_SETPOINT,sP);

	}

	@Override
	public int getDelay() {
		return pid.getDelay();
	}

	@Override
	public void setDelay(int delay) {
		pid.setDelay(delay);

	}

	@Override
	public boolean isIntegralFrozen() {
		return pid.isIntegralFrozen();
	}

	@Override
	public void setIntegralFrozen(boolean integralFrozen) {
		pid.freezeIntegral(integralFrozen);
	}
	
	/**
	 * Attempt to save parametes in a file with specified filename.
	 * 
	 * @param filename Filename to use
	 * @return True if save was succesful, false if not. 
	 */
	public boolean saveToFile(String filename) {
		File file = new File(filename);
		FileOutputStream fileOut = null;

		try {
			if (!file.exists()) {
				file.createNewFile();
			}
			fileOut = new FileOutputStream(file);
			DataOutputStream dataOut = new DataOutputStream(fileOut);
			dataOut.writeInt(this.getDelay());
			dataOut.writeFloat(this.getSP());
			dataOut.writeFloat(this.getKp());
			dataOut.writeFloat(this.getKi());
			dataOut.writeFloat(this.getKd());
			dataOut.writeFloat(this.getMVHighLimit());
			dataOut.writeFloat(this.getMVLowLimit());
			dataOut.writeFloat(this.getIntegralWindupHighLimit());
			dataOut.writeFloat(this.getIntegralWindupLowLimit());
			dataOut.writeFloat(this.getMVDeadband());
			dataOut.writeFloat(this.getRampExponent());
			dataOut.writeFloat(this.getRampTriggerThreshold());
			LCD.clear();
			LCD.drawString("Params saved", 0, 0);
			LCD.drawString("to:", 0, 1);
			LCD.drawString(filename, 0, 2);
			LCD.refresh();
			Button.waitForAnyPress();
			LCD.clear();
			dataOut.close();
			fileOut.close();
			return true;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return false;
	}
	
	/**
	 * Attempt to load parameters from a file with specified filename
	 * @param filename Filename to load persisted values from
	 * @return True if loading was succesful, otherwise false.
	 */
	public boolean loadFromFile(String filename) {
		File file = new File(filename);
		FileInputStream fileIn = null;

		try {
			if (!file.exists()) {
				throw new FileNotFoundException("File not found");
			}
			fileIn = new FileInputStream(file);
			DataInputStream dataIn = new DataInputStream(fileIn);
			setDelay(dataIn.readInt());
			setSP(dataIn.readFloat());
			setKp(dataIn.readFloat());
			setKi(dataIn.readFloat());
			setKd(dataIn.readFloat());
			setMVHighLimit(dataIn.readFloat());
			setMVLowLimit(dataIn.readFloat());
			setIntegralWindupHighLimit(dataIn.readFloat());
			setIntegralWindupLowLimit(dataIn.readFloat());
			setMVDeadband(dataIn.readFloat());
			setRampExponent(dataIn.readFloat());
			setRampTriggerThreshold(dataIn.readFloat());
			dataIn.close();
			fileIn.close();
			return true;
		} catch (IOException e) {
			e.printStackTrace();
		} finally {

		}
		return false;
	}
}
