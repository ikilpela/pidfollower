package pidfollower;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import org.lejos.util.PIDTuningProvider;

import lejos.nxt.LCD;
import lejos.nxt.Motor;
import lejos.nxt.Sound;
import lejos.nxt.comm.NXTConnection;
import lejos.robotics.navigation.DifferentialPilot;
import lejos.util.PilotProps;

public class BTConnectionManager implements Runnable {
	
	private volatile boolean closed = false;
	
	private NXTConnection conn;
	private PIDFollower pidFollower;
	private PIDTuningProvider tuner;
	private DifferentialPilot pilot;
	private DataInputStream dataIn;
	private DataOutputStream dataOut;
	
	
	/**
	 * Constructor. Creates a new BTConnectionManager using provided NXTConnection and PIDFollower instance
	 * 
	 * @param connection Connection to use for data transfer.
	 * @param pidFollower PIDFollower instance to pass parameters to. 
	 */
	public BTConnectionManager(NXTConnection connection, PIDFollower pidFollower) {
		this.conn = connection;
		this.pidFollower = pidFollower;
		this.tuner = pidFollower.getTuningProvider();
		this.pilot = pidFollower.getPilot();
		dataIn = conn.openDataInputStream();
		dataOut = conn.openDataOutputStream();
	}
	
	/**
	 * Sends the PIDFollower state to PC app
	 * 
	 */
	public void sendPIDFollowerState(int MV, int lightValue) {
		synchronized (dataOut) {
			try {
				dataOut.write(StreamHeaders.SEND_FOLLOWER_STATE_CMD);
				dataOut.writeInt(Motor.A.getRotationSpeed());
				dataOut.writeInt(Motor.C.getRotationSpeed());
				dataOut.writeInt(MV);
				dataOut.writeInt(lightValue);
				dataOut.flush();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Sends "Follower started" message to PC app
	 */
	public void sendFollowerStarted() {
		synchronized (dataOut) {
			try {
				dataOut.write(StreamHeaders.SEND_FOLLOWER_STARTED_CMD);
				dataOut.flush();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Sends "Follower stopped" message to PC app
	 */
	public void sendFollowerStopped() {
		synchronized (dataOut) {
			try {
				dataOut.write(StreamHeaders.SEND_FOLLOWER_STOPPED_CMD);
				dataOut.flush();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Closes all connections and stops listening for packets
	 */
	public void close() {
		this.closed = true;
	}
	
	
	/**
	 * Runs on separate thread and listens to packets sent via bluetooth. 
	 * Action taken is decided by the received header byte.  
	 */
	@Override
	public void run() {
		try {
			while (!closed) {
				byte cmd = dataIn.readByte();
				switch (cmd) {
				
					/**
					 *  Replies with all PidTuner parameters in preassigned order
					 */
					case StreamHeaders.NXT_POLL_PID_CMD:
						synchronized(dataOut) {
							dataOut.writeByte(StreamHeaders.NXT_POLL_PID_CMD);
							dataOut.writeInt(tuner.getDelay());
							
							dataOut.writeFloat(tuner.getKp());
							dataOut.writeFloat(tuner.getKd());
							dataOut.writeFloat(tuner.getKi());
							
							dataOut.writeFloat(tuner.getMVHighLimit());
							dataOut.writeFloat(tuner.getMVLowLimit());
							dataOut.writeFloat(tuner.getIntegralWindupHighLimit());
							dataOut.writeFloat(tuner.getIntegralWindupLowLimit());
							
							dataOut.writeFloat(tuner.getMVDeadband());
							dataOut.writeFloat(tuner.getRampExponent());
							dataOut.writeFloat(tuner.getRampTriggerThreshold());
							dataOut.flush();
						}
					break;
					
					/**
					 * Sets PIDController parameters to values read from the BT inputstream.
					 */
					case StreamHeaders.SEND_PID_VALUES_CMD:
						synchronized(dataIn) {
							tuner.setDelay(dataIn.readInt());
							tuner.setKp(dataIn.readFloat());
							tuner.setKd(dataIn.readFloat());
							tuner.setKi(dataIn.readFloat());
							tuner.setMVHighLimit(dataIn.readFloat());
							tuner.setMVLowLimit(dataIn.readFloat());
							tuner.setIntegralWindupHighLimit(dataIn.readFloat());
							tuner.setIntegralWindupLowLimit(dataIn.readFloat());
							tuner.setMVDeadband(dataIn.readFloat());
							tuner.setRampExponent(dataIn.readFloat());
							tuner.setRampTriggerThreshold(dataIn.readFloat());
							Sound.beepSequenceUp();
						}
					break;
					
					/**
					 * Persists received pilot properties so they are used next time NXT is started.
					 */
					case StreamHeaders.SEND_PILOT_PROPS_CMD:
						
						float wheelDiameter = dataIn.readFloat();
						float trackWidth = dataIn.readFloat();
						char motorLeft = dataIn.readChar();
						char motorRight = dataIn.readChar();

						
						// Create pilotprops instance and store values in it
						PilotProps savedProps = new PilotProps();
						savedProps.setProperty(PilotProps.KEY_WHEELDIAMETER, String.valueOf(wheelDiameter));
						savedProps.setProperty(PilotProps.KEY_TRACKWIDTH, String.valueOf(trackWidth));
						savedProps.setProperty(PilotProps.KEY_LEFTMOTOR, String.valueOf(motorLeft));
						savedProps.setProperty(PilotProps.KEY_RIGHTMOTOR, String.valueOf(motorRight));
						
						// Persist the PilotProps so it gets loaded on next startup;
						savedProps.storePersistentValues();
						// Play sound to confirm values were received succesfully
						Sound.beepSequenceUp();
						
						break;
					
					/**
					 *  Replies with all pilot related values in preassigned order
					 */
					case StreamHeaders.NXT_POLL_PILOT_CMD:
						synchronized(dataOut) {
							dataOut.writeByte(StreamHeaders.NXT_POLL_PILOT_CMD); // Header
							dataOut.writeInt((int) pilot.getTravelSpeed()); // Travel speed
							dataOut.writeInt(0); // Acceleration (cannot be read from pilot)
							dataOut.writeInt((int) pilot.getRotateSpeed()); // Rotate speed
							
							PilotProps loadedProps = new PilotProps(); 
							loadedProps.loadPersistentValues();
							
							dataOut.writeFloat(Float.valueOf(loadedProps.getProperty(PilotProps.KEY_WHEELDIAMETER, "0"))); // Wheel diameter
							dataOut.writeFloat(Float.valueOf(loadedProps.getProperty(PilotProps.KEY_TRACKWIDTH, "0"))); // Track width
							dataOut.writeChar(loadedProps.getProperty(PilotProps.KEY_LEFTMOTOR,"C").toCharArray()[0]); // Left motor
							dataOut.writeChar(loadedProps.getProperty(PilotProps.KEY_RIGHTMOTOR,"A").toCharArray()[0]); // Right motor
							dataOut.flush();
						}
					break;
					
					/**
					 * Reads single pilot param from stream. The param is denoted with a specific header byte so 
					 * listener knows which parameter is received.
					 */
					case StreamHeaders.SEND_SINGLE_PILOT_PARAM_CMD:
						
						synchronized(dataIn) {	
							
							byte paramType = dataIn.readByte();
							
							switch (paramType) {
							case StreamHeaders.PILOT_PARAM_SPEED:
								pilot.setTravelSpeed(dataIn.readInt());
								break;
							case StreamHeaders.PILOT_PARAM_ACCELERATION:
								pilot.setTravelSpeed(dataIn.readInt());
								break;
							case StreamHeaders.PILOT_PARAM_ROTATESPEED:
								pilot.setTravelSpeed(dataIn.readInt());
								break;
							}
						}
						break;
					
					/**
					 * Stops the PIDFollower.
					 */
					case StreamHeaders.STOP_PID_CMD: 
						pidFollower.stop();
						break;
					
					default:
						LCD.drawString("Fail!", 0, 0);
					break;
				}
			}
		} catch (IOException ex) {
			
		} finally {
			try {
				dataIn.close();
				dataOut.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
