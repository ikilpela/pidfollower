package rcbot;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import lejos.nxt.Button;
import lejos.nxt.LCD;
import lejos.nxt.Motor;
import lejos.nxt.Sound;
import lejos.nxt.comm.Bluetooth;
import lejos.nxt.comm.NXTConnection;
import lejos.robotics.navigation.DifferentialPilot;

public class RCBot {
	
	private double SPEED_DELTA = 1;
	
	public static void main(String[] args) {
		DifferentialPilot pilot = new DifferentialPilot(4.3f,10.5f,Motor.A,Motor.C);
		pilot.setTravelSpeed(5d);
		RCBot bot = new RCBot();
		bot.start(pilot);
	}
	
	public void start(DifferentialPilot pilot) {
		
		LCD.drawString("Waiting..",0,0);
		NXTConnection conn = Bluetooth.waitForConnection();
		LCD.drawString("Connected!", 0, 0);
		DataInputStream input = conn.openDataInputStream();
		DataOutputStream output = conn.openDataOutputStream();
		LCD.drawString("Waiting for", 1, 2);
		LCD.drawString("command",1,3);
		
		boolean cancel = false; 
		try {
			while (!Button.ESCAPE.isDown() && !cancel) {
				int command = input.readByte();
				LCD.drawString("Command: " + command, 0, 5);				
				switch(command) {
					case BTCommands.CMD_CANCEL:
						cancel = true;
						break;
					case BTCommands.CMD_STEER:
						Sound.beep();
						int value = input.readInt();
						pilot.steer(value);
						break;
					case BTCommands.CMD_SPEED:
						Sound.beep();
						double newSpeed = input.readInt();
						double speed = Math.abs(newSpeed) < pilot.getMaxTravelSpeed() ? newSpeed : Math.signum(newSpeed) * pilot.getMaxTravelSpeed();
						pilot.setTravelSpeed(speed);
						
						if(!pilot.isMoving()) {
							pilot.forward();
						}
						break;
					case BTCommands.CMD_STOP:
						pilot.stop();
						break;
					default:
//						try {
//							Thread.sleep(100);
//						} catch (InterruptedException e) {
//							// TODO Auto-generated catch block
//							e.printStackTrace();
//						}
						break;					
					}
				} // End while block
//			conn.close();
//			LCD.clear();
//			LCD.drawString("Conn closed", 0, 0);
		} catch (IOException e) {
			Sound.beep();
			e.printStackTrace();
		} finally {
			try {
				input.close();
				output.close();
				conn.close();
				LCD.clear();
				LCD.drawString("Conn closed", 0, 0);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}				
		}

	}
}
