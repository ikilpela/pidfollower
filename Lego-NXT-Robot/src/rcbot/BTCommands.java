package rcbot;

public class BTCommands {
	
	public static final byte CMD_CANCEL = 0;
	public static final byte CMD_STEER = 1;
	public static final byte CMD_SPEED = 2;
	public static final byte CMD_STOP = 3;
	
}
