package linefollower;

import lejos.nxt.LCD;
import lejos.nxt.Sound;

public abstract class Utils {
	
	/**
	 * Initializes a beep countdown.
	 * @param seconds How many 1 second beeps before final note is played
	 */
	public static void countdown(int seconds) {
		for (int i = 0; i < 3; i++) {
			try {
				Sound.playTone(440, 500);
				LCD.drawInt(3-i, 7, 4);
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		Sound.playTone(880,1000);
	}
	
	/**
	 * Splits string in 15 char rows and prints it on LCD display starting from line 0.
	 * Doesn't take spaces into account when splitting.
	 * @param str String to draw on LCD
	 */
	public static void drawString(String str) {
		int rowLength = 15;
		for (int i = 0; i*rowLength < str.length();i++) {
			LCD.drawString(str.substring(i*rowLength,Math.min((i+1)*rowLength,str.length()-1)), 0, i);
		}
		
		
	}
}
