package lightsensor;

import java.util.ArrayList;

import lejos.nxt.Button;
import lejos.nxt.LCD;
import lejos.nxt.LightSensor;
import lejos.nxt.SensorPort;
import lejos.robotics.navigation.DifferentialPilot;

public class Calibrator {


	public static void main(String[] args) {
		LightSensor light = new LightSensor(SensorPort.S1);
		calibrate(light);
	}
	
	/**
	 * Calibrates light sensor with NXT button presses. Press LEFT to set low value (black) and RIGHT to set high (white).
	 * After calibrating press ESCAPE to return from method.
	 * 
	 * @param LigthSensor instance to calibrate
	 */
	public static void calibrate(LightSensor light) {
		LCD.drawString("Calibrating", 0, 0);
		LCD.drawString("L for low", 1, 5);
		LCD.drawString("R for high", 1, 6);
		LCD.drawString("Low:" + light.getLow(),1,1);
		LCD.drawString("High: " + light.getHigh(), 1, 2);
		LCD.drawString("0-100: ",1,3);
		LCD.drawString("Oma:",1,4);
		LCD.drawString("Raw: ", 4, 7);
		while (!Button.ESCAPE.isDown()) {
			LCD.drawInt(light.getNormalizedLightValue(),4, 9, 7);
			LCD.drawInt(light.getLightValue(),4, 8, 3);
			LCD.drawInt(getCalibratedLightValue(light),4, 7, 4);
			if (Button.LEFT.isDown()) {
				light.setLow(light.getNormalizedLightValue());
				LCD.clear(1);
				LCD.drawString("Low:" + light.getLow(),1,1);
			} else if (Button.RIGHT.isDown()) {
				light.setHigh(light.getNormalizedLightValue());
				LCD.clear(2);
				LCD.drawString("High:" + light.getHigh(),1,2);
			}
		}
		LCD.clear();
		LCD.drawString("Calibration",0,0);
		LCD.drawString("complete", 0, 1);
		LCD.refresh();
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		LCD.clear();
	}
	
	/**
	 * Attempts to automatically calibrate high and low values for LightSensor instance by rotating pilot.
	 * Waits for any press before returning from method.
	 * 
	 * @param light LightSensor to calibrate
	 * @param pilot DifferentialPilot used to control the NXT
	 */
	public static void autoCalibrate(LightSensor light, DifferentialPilot pilot) {
		LCD.drawString("Autocalibrate", 0, 0);
		LCD.drawString("Press any button", 0, 1);
		Button.waitForAnyPress();
		ArrayList<Integer> values = new ArrayList<Integer>();
		Thread t = new Thread(new Runnable() {			
			public void run() {
				long startTime = System.currentTimeMillis();
				while(System.currentTimeMillis() - startTime < 500) {
					values.add(light.getNormalizedLightValue());
				}
			}
		});

		t.start();

		double origRotateSpeed = pilot.getRotateSpeed();
		pilot.setRotateSpeed(50);
		pilot.rotate(45);
		pilot.rotate(-90);
		pilot.rotate(45);
		pilot.setRotateSpeed(origRotateSpeed);
		for (Integer i : values) {
			if (i < light.getLow()) light.setLow(i);
			else if (i > light.getHigh()) light.setHigh(i);
		}
		LCD.drawInt(values.size(),4,1,7);
		LCD.drawString("Low: " + light.getLow(), 1, 1);
		LCD.drawString("High: " +light.getHigh(), 1, 2);
		Button.waitForAnyPress();
	}

	/**
	 * Returns a normalized light value so that center of the calibrated range is at 0, darker values are < 0 and lighter are > 0. 
	 * 
	 * @param light Lightsensor to use
	 * @return Normalized lightsensor reading
	 */
	public static int getCalibratedLightValue(LightSensor light) {
		float zero = (float)(light.getHigh()+light.getLow())/2;
		int val = (int) (light.getNormalizedLightValue() - zero);
		return val;
	}
}
